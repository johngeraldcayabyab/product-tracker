<?php


Route::get('/', 'LoginCont@index')->name('login');
Route::get('/login', 'LoginCont@index');

Route::post('/loginProcess', 'LoginCont@loginProcess')->name('loginProcess');


Route::middleware(['checkIfLoggedIn'])->group(function () {

    /**
     * Dashboard
     */
    Route::prefix('/dashboard')->group(function(){
        Route::get('/home', 'Dashboard\HomeCont@showHome')->name('showHome');
        Route::get('/logout', 'LogOutCont@logOut')->name('logOut');
        Route::post('/company_logo/update', 'Dashboard\HomeCont@processUpdateCompanyLogo')->name('processUpdateCompanyLogo');
    });

    /**
     * Admin
     */
    Route::prefix('/dashboard/settings')->group(function(){
        // Company Profile Settings


        // Currency settings
        Route::get('curreny_settings', 'Dashboard\SettingsCont@showCurrencySettings')->name('showCurrencySettings');
        Route::post('curreny_settings/create', 'Dashboard\SettingsCont@processCreateCurrency')->name('processCreateCurrency');
        Route::post('curreny_settings/update', 'Dashboard\SettingsCont@processUpdateCurrency')->name('processUpdateCurrency');
        Route::post('curreny_settings/delete', 'Dashboard\SettingsCont@processDeleteCurrency')->name('processDeleteCurrency');
        Route::post('curreny_settings/active', 'Dashboard\SettingsCont@processActivateCurrency')->name('processActivateCurrency');


        // Units of measurement settings
        Route::get('unit_of_measurement_type/select', 'Dashboard\SettingsCont@showSelectUnitOfMeasurementType')->name('showSelectUnitOfMeasurementType');
        Route::post('unit_of_measurement_type/process', 'Dashboard\SettingsCont@processSelectUnitOfMeasurementType')->name('processSelectUnitOfMeasurementType');

        Route::get('units_of_measurement', 'Dashboard\SettingsCont@showUnitsOfMeasurement')->name('showUnitsOfMeasurement');
        Route::post('units_of_measurement/create', 'Dashboard\SettingsCont@processCreateUnitOfMeasurement')->name('processCreateUnitsOfMeasurement');
        Route::post('units_of_measurement/update', 'Dashboard\SettingsCont@processUpdateUnitOfMeasurement')->name('processUpdateUnitOfMeasurement');
        Route::post('units_of_measurement/delete', 'Dashboard\SettingsCont@processDeleteUnitOfMeasurement')->name('processDeleteUnitOfMeasurement');

        // Product categories settings
        Route::get('product_settings/categories', 'Dashboard\SettingsCont@showProductSettingsCategories')->name('showProductSettingsCategories');
        Route::post('product_settings/categories/create', 'Dashboard\SettingsCont@processCreateProductCategory')->name('processCreateProductCategory');
        Route::post('product_settings/categories/update', 'Dashboard\SettingsCont@processUpdateProductCategory')->name('processUpdateProductCategory');
        Route::post('product_settings/categories/delete', 'Dashboard\SettingsCont@processDeleteProductCategory')->name('processDeleteProductCategory');

        // User settings
        Route::get('user_settings', 'Dashboard\SettingsCont@showUserSettings')->name('showUserSettings');
        Route::post('user_settings/create', 'Dashboard\SettingsCont@processCreateUser')->name('processCreateUser');
        Route::post('user_settings/edit', 'Dashboard\SettingsCont@processEditUser')->name('processEditUser');
        Route::post('user_settings/delete', 'Dashboard\SettingsCont@processDeleteUser')->name('processDeleteUser');
    });


    /**
     * Products link
     */
    Route::prefix('/dashboard/inventory')->group(function(){
        Route::get('/main', 'Dashboard\InventoryCont@showInventory')->name('showInventory');

        /**
         * Products
         */
        Route::get('/products', 'Dashboard\InventoryCont@showProducts')->name('showProducts');
        Route::get('/products/create_and_edit/{iId}', 'Dashboard\InventoryCont@showCreateProducts')->name('showCreateProducts');
        Route::post('/products/create', 'Dashboard\InventoryCont@processCreateProducts')->name('processCreateProducts');
        Route::get('/products/edit/{iId}', 'Dashboard\InventoryCont@showEditProducts')->name('showEditProducts');
        Route::post('/products/edit', 'Dashboard\InventoryCont@processEditProducts')->name('processEditProducts');
        Route::post('/products/delete', 'Dashboard\InventoryCont@processDeleteProducts')->name('processDeleteProducts');

    });


    Route::prefix('/dashboard/contacts')->group(function(){
        Route::get('/customers', 'Dashboard\ContactsCont@showCustomers')->name('showCustomers');
        Route::get('/customers/create', 'Dashboard\ContactsCont@showCreateCustomer')->name('showCreateCustomers');
        Route::post('/customers/create/process', 'Dashboard\ContactsCont@processCreateCustomer')->name('processCreateCustomers');
        Route::get('/customers/edit/{iId}', 'Dashboard\ContactsCont@showEditCustomer')->name('showEditCustomers');
        Route::post('/customers/edit/process', 'Dashboard\ContactsCont@processEditCustomer')->name('processEditCustomers');
        Route::post('/customers/delete/process', 'Dashboard\ContactsCont@processDeleteCustomer')->name('processDeleteCustomers');
    });


    Route::prefix('/dashboard/profile')->group(function(){
        Route::get('/', 'Dashboard\ProfileCont@showUserProfile')->name('showUserProfile');
        Route::post('/update_profile', 'Dashboard\ProfileCont@processUpdateProfile')->name('processUpdateProfile');
        Route::post('/update_password', 'Dashboard\ProfileCont@processUpdatePassword')->name('processUpdatePassword');
    });




});



// testing purposes
Route::get('/test', 'Test@index');


