<?php

use App\Http\Models\RawMaterialsModel;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', 'Api\ProductsApi@apiGetProducts')->name('getProducts');
Route::get('/product_names_and_quantity', 'Api\ProductsApi@apiGetNamesAndQuantity')->name('getProductNamesAndQuantity');
