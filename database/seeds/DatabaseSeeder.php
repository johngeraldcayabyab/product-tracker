<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            ProductCategoriesSeeder::class,
            ProductsSeeder::class,
            CurrenciesSeeder::class
        ]);
    }

}
