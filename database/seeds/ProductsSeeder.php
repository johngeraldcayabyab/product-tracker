<?php

use App\Http\Models\ProductsModel;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    public function run()
    {
        $aProducts =
        [
            [
                'name' => 'burger',
                'sales_price' => 100,
                'cost_price' => 90,
                'measurement_type' => null,
                'product_type' => 'Stockable',
                'product_category' => 1,
                'image_name' => null,
                'quantity_on_hand' => 10,
                'sku' => 123,
                'upc' => 123
            ],
            [
                'name' => 'hotdog',
                'sales_price' => 25,
                'cost_price' => 20,
                'measurement_type' => null,
                'product_type' => 'Consumable',
                'product_category' => 1,
                'image_name' => null,
                'quantity_on_hand' => 10,
                'sku' => 33,
                'upc' => 12345
            ],
            [
                'name' => 'massage',
                'sales_price' => 100,
                'cost_price' => 100,
                'measurement_type' => null,
                'product_type' => 'Service',
                'product_category' => 2,
                'image_name' => null,
                'quantity_on_hand' => null,
                'sku' => null,
                'upc' => null
            ],
            [
                'name' => 'lemon',
                'sales_price' => 100,
                'cost_price' => 80,
                'measurement_type' => null,
                'product_type' => 'Stockable',
                'product_category' => null,
                'image_name' => null,
                'quantity_on_hand' => 50,
                'sku' => null,
                'upc' => null
            ],
            [
                'name' => 'alcohol',
                'sales_price' => 100,
                'cost_price' => 50,
                'measurement_type' => null,
                'product_type' => 'Stockable',
                'product_category' => 1,
                'image_name' => null,
                'quantity_on_hand' => 100,
                'sku' => 666,
                'upc' => 787
            ],
            [
                'name' => 'kropek',
                'sales_price' => 100,
                'cost_price' => 50,
                'measurement_type' => null,
                'product_type' => 'Stockable',
                'product_category' => 1,
                'image_name' => null,
                'quantity_on_hand' => 100,
                'sku' => 6166,
                'upc' => 2787
            ],
            [
                'name' => 'tilapia',
                'sales_price' => 100,
                'cost_price' => 50,
                'measurement_type' => null,
                'product_type' => 'Stockable',
                'product_category' => 1,
                'image_name' => null,
                'quantity_on_hand' => 100,
                'sku' => 6612323236,
                'upc' => 7822227
            ],
            [
                'name' => 'toothpaste',
                'sales_price' => 100,
                'cost_price' => 50,
                'measurement_type' => null,
                'product_type' => 'Consumable',
                'product_category' => 1,
                'image_name' => null,
                'quantity_on_hand' => 100,
                'sku' => 666123123123,
                'upc' => 123123123787
            ],
            [
                'name' => 'monitor',
                'sales_price' => 100,
                'cost_price' => 50,
                'measurement_type' => null,
                'product_type' => 'Stockable',
                'product_category' => 1,
                'image_name' => null,
                'quantity_on_hand' => 100,
                'sku' => 666,
                'upc' => 787
            ],
            [
                'name' => 'Deliver food',
                'sales_price' => 100,
                'cost_price' => 0,
                'measurement_type' => null,
                'product_type' => 'Service',
                'product_category' => 2,
                'image_name' => null,
                'quantity_on_hand' => 100,
                'sku' => 66206,
                'upc' => 71232387
            ],
            [
                'name' => 'Sing',
                'sales_price' => 100,
                'cost_price' => 0,
                'measurement_type' => null,
                'product_type' => 'Service',
                'product_category' => 2,
                'image_name' => null,
                'quantity_on_hand' => 100,
                'sku' => 61345666,
                'upc' => 78711111
            ],
        ];

        foreach($aProducts as $aProduct)
        {
            $oProductsModel = new ProductsModel();
            $oProductsModel->name = $aProduct['name'];
            $oProductsModel->sales_price = $aProduct['sales_price'];
            $oProductsModel->cost_price = $aProduct['cost_price'];
            $oProductsModel->measurement_type = $aProduct['measurement_type'];
            $oProductsModel->product_type = $aProduct['product_type'];
            $oProductsModel->product_category = $aProduct['product_category'];
            $oProductsModel->image_name = $aProduct['image_name'];
            $oProductsModel->quantity_on_hand = $aProduct['quantity_on_hand'];
            $oProductsModel->sku = $aProduct['sku'];
            $oProductsModel->upc = $aProduct['upc'];
            $oProductsModel->save();
        }
    }

}
