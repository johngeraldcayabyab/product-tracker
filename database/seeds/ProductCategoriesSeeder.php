<?php

use App\Http\Models\ProductCategoriesModel;
use Illuminate\Database\Seeder;

class ProductCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aProductCategories =
            [
                'food',
                'not food',
            ];

        foreach($aProductCategories as $sProductCategory)
        {
            $oProductCategoriesModel = new ProductCategoriesModel();
            $oProductCategoriesModel->name = $sProductCategory;
            $oProductCategoriesModel->save();
        }
    }
}
