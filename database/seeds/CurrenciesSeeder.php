<?php
use App\Http\Models\CurrenciesModel;
use Illuminate\Database\Seeder;

/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 6/25/2018
 * Time: 5:03 PM
 */
class CurrenciesSeeder extends Seeder
{

    public function run()
    {
        $aCurrencies =
        [
            [
                'name' => 'US Dollar',
                'abbr' => 'USD',
                'symbol' => '$',
            ],
            [
                'name' => 'Philippine peso',
                'abbr' => 'PHP',
                'symbol' => '₱',
            ]
        ];

        foreach($aCurrencies as $aCurrency)
        {
            $oCurrenciesModel = new CurrenciesModel();
            $oCurrenciesModel->name = $aCurrency['name'];
            $oCurrenciesModel->abbr = $aCurrency['abbr'];
            $oCurrenciesModel->symbol = $aCurrency['symbol'];
            $oCurrenciesModel->save();
        }

    }
}