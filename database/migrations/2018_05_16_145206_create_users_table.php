<?php

use App\Http\Models\UsersModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 255);
            $table->string('password', 255);
            $table->string('name', 255)->nullable();
            $table->string('remember_token', 255)->nullable();
            $table->timestamps();
        });

        $aUsers =
            [
                [
                    'username' => 'admin',
                    'password' => Hash::make('admin'),
                    'name' => 'admin',
                ]
            ];

        foreach($aUsers as $aUser){
            $oUsersModel = new UsersModel();
            $oUsersModel->username     = $aUser['username'];
            $oUsersModel->password     = $aUser['password'];
            $oUsersModel->name     = $aUser['name'];
            $oUsersModel->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
