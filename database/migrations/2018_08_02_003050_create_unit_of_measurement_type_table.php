<?php

use App\Http\Models\UnitsOfMeasurementModel;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitOfMeasurementTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_of_measurement_type', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['metric', 'imperial'])->nullable()->default(null);
            $table->timestamps();
        });

        $aUnitsOfMeasurements =
            [
                [
                    'name'                     => 'grain',
                    'abbr'                     => 'gr',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'drachm',
                    'abbr'                     => 'dr',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'ounce',
                    'abbr'                     => 'oz',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'pound',
                    'abbr'                     => 'lb',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'stone',
                    'abbr'                     => 'st',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'quarter',
                    'abbr'                     => 'qtr',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'hundredweight',
                    'abbr'                     => 'cwt',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'ton',
                    'abbr'                     => 't',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'cubic foot',
                    'abbr'                     => 'ft<sup>3</sup>',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'volume',
                ],
                [
                    'name'                     => 'cubic yard',
                    'abbr'                     => 'yd<sup>3</sup>',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'volume',
                ],
                [
                    'name'                     => 'pint',
                    'abbr'                     => 'pt',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'volume',
                ],
                [
                    'name'                     => 'gallon',
                    'abbr'                     => 'gal',
                    'unit_of_measurement_type' => 'imperial',
                    'measurement_type'         => 'volume',
                ],
                [
                    'name'                     => 'gram',
                    'abbr'                     => 'g',
                    'unit_of_measurement_type' => 'metric',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'metric carat',
                    'abbr'                     => 'ct',
                    'unit_of_measurement_type' => 'metric',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'kilogram',
                    'abbr'                     => 'kg',
                    'unit_of_measurement_type' => 'metric',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'tonne',
                    'abbr'                     => 't',
                    'unit_of_measurement_type' => 'metric',
                    'measurement_type'         => 'mass',
                ],
                [
                    'name'                     => 'cubic decimetre',
                    'abbr'                     => 'dm<sup>3</sup>',
                    'unit_of_measurement_type' => 'metric',
                    'measurement_type'         => 'volume',
                ],
                [
                    'name'                     => 'cubic metre',
                    'abbr'                     => 'm<sup>3</sup>',
                    'unit_of_measurement_type' => 'metric',
                    'measurement_type'         => 'volume',
                ],
                [
                    'name'                     => 'litre',
                    'abbr'                     => 'l',
                    'unit_of_measurement_type' => 'metric',
                    'measurement_type'         => 'volume',
                ],
                [
                    'name'                     => 'hectolitre',
                    'abbr'                     => 'hl',
                    'unit_of_measurement_type' => 'metric',
                    'measurement_type'         => 'volume',
                ],
                [
                    'name'                     => 'pieces',
                    'abbr'                     => 'pcs',
                    'unit_of_measurement_type' => 'pieces',
                    'measurement_type'         => 'pieces',
                    'active'                   => true
                ]
            ];

        foreach ($aUnitsOfMeasurements as $aUnitsOfMeasurement)
        {
            $oUnitsOfMeasurementsModel = new UnitsOfMeasurementModel();
            $oUnitsOfMeasurementsModel->name = $aUnitsOfMeasurement['name'];
            $oUnitsOfMeasurementsModel->abbr = $aUnitsOfMeasurement['abbr'];
            $oUnitsOfMeasurementsModel->unit_of_measurement_type = $aUnitsOfMeasurement['unit_of_measurement_type'];
            $oUnitsOfMeasurementsModel->measurement_type = $aUnitsOfMeasurement['measurement_type'];
            $oUnitsOfMeasurementsModel->active = isset($aUnitsOfMeasurement['active']) ? $aUnitsOfMeasurement['active'] : false;
            $oUnitsOfMeasurementsModel->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_of_measurement_type');
    }
}
