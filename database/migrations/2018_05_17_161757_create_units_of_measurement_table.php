<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsOfMeasurementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units_of_measurement', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('abbr', 255); // abbreviation
            $table->enum('unit_of_measurement_type', ['metric', 'imperial', 'pieces']);
            $table->enum('measurement_type', ['mass', 'volume', 'pieces']);
            $table->boolean('active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units_of_measurement');
    }
}
