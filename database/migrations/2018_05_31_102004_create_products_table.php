<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Stockable = has quantity
        // Consumable = no need to track of quantity, meaning that it is always available
        // Service = well, it's a service for christ sake

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->decimal('sales_price', 65, 30)->nullable()->default(null);
            $table->decimal('cost_price', 65, 30)->nullable()->default(null);
            $table->unsignedInteger('measurement_type')->nullable()->default(null);
            $table->foreign('measurement_type')->references('id')->on('units_of_measurement');
            $table->enum('product_type', ['Stockable', 'Consumable', 'Service'])->nullable()->default(null);
            $table->unsignedInteger('product_category')->nullable()->default(null);
            $table->foreign('product_category')->references('id')->on('product_categories');
            $table->string('image_name', 255)->nullable()->default(null);
            $table->decimal('quantity_on_hand', 65, 30)->nullable()->default(null);
            $table->string('sku', 255)->nullable()->default(null);
            $table->string('upc', 255)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
