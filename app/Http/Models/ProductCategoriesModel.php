<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 5/31/2018
 * Time: 5:58 PM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class ProductCategoriesModel extends Model
{
    protected $table = 'product_categories';

// loads only direct children - 1 level
    public function children()
    {
        return $this->hasMany('App\Http\Models\ProductCategoriesModel', 'parent_category_id');
    }

// recursive, loads all descendants
    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
        // which is equivalent to:
        // return $this->hasMany('App\Http\Models\ProductCategoriesModel', 'parent')->with('childrenRecursive);
    }

// parent
    public function parent()
    {
        return $this->belongsTo('App\Http\Models\ProductCategoriesModel','parent_category_id');
    }

// all ascendants
    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

}