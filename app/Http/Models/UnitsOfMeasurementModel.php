<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 6/6/2018
 * Time: 12:04 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class UnitsOfMeasurementModel extends Model
{
    protected $table = 'units_of_measurement';

    public function products()
    {
        return $this->belongsTo('App\Http\Model\ProductsModel')->as('measurementType');
    }
}