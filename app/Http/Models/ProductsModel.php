<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 6/5/2018
 * Time: 1:06 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class ProductsModel extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'measurement_type', 'product_type', 'product_category'];


    public function bill_of_materials()
    {
        return $this->belongsTo('App\Http\Models\BillOfMaterialsModel', 'product_id', 'id');
    }

    public function measurementType()
    {
        return $this->hasOne('App\Http\Models\UnitsOfMeasurementModel', 'id', 'measurement_type');
    }

}