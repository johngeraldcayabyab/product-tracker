<?php
/**
 * Created by John Gerald B. Cayabyab. | johngeraldcayabyab@gmail.com
 * Date: 02/08/2018
 * Time: 8:40 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class UnitOfMeasurementTypeModel extends Model
{
    protected $table = 'unit_of_measurement_type';
}