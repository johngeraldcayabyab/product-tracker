<?php
/**
 * Created by John Gerald B. Cayabyab. | johngeraldcayabyab@gmail.com
 * Date: 15/08/2018
 * Time: 10:28 AM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class LogsModel extends Model
{
    protected $table = 'logs';
}