<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 6/10/2018
 * Time: 10:01 PM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class BillOfMaterialsModel extends Model
{
    protected $table = 'bill_of_materials';

    protected $fillable = [];

    public function products()
    {
        return $this->hasOne('App\Http\Models\ProductsModel', 'id', 'product_id');
    }

}