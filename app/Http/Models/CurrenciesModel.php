<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 6/25/2018
 * Time: 4:59 PM
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class CurrenciesModel extends Model
{
    protected $table = 'currencies';

    public function companyInformation()
    {
        return $this->belongsTo('App\Http\Model\CompanyInformationModel')->as('currency');
    }
}