<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 5/31/2018
 * Time: 1:25 AM
 */

namespace App\Http\Controllers;


use App\Http\Business_Logic\Traits\LogsTrait;
use Illuminate\Support\Facades\Auth;

class LogOutCont
{
    use LogsTrait;

    public function logOut()
    {
        LogsTrait::create(auth()->user()->name, 'Logged out');
        Auth::logout();
        session()->forget('userType');
        return redirect()->route('login');
    }
}