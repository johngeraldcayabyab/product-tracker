<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 7/4/2018
 * Time: 7:17 PM
 */

namespace App\Http\Controllers;


use App\Http\Business_Logic\ProductsBL;
use App\Http\Business_Logic\UnitsOfMeasurementBL;
use App\Http\Controllers\Dashboard\BaseCont;

class Test extends BaseCont
{

    public function index()
    {
//        $oProductsBL = new ProductsBL();
//        $oProductTypes = $oProductsBL->getAllProductTypes();
//        $oProductCategories = $oProductsBL->getAllProductCategories();
        return view('test');
    }


//    public function showRemitRequests(Request $request)
//    {
//        $remitRequests = new RemitRequests();
//        $totalRemitRequests = $remitRequests::count();
//        $totalPendingRemitRequests = $remitRequests::where('sent', 0)->count();
//        $pendingRemitRequests = $remitRequests::whereDate('created_at', Carbon::today())->orWhere('sent', 0);
//        $totalRemitAmountUsd = $pendingRemitRequests->sum('remitamt');
//        $totalRemitFees = $pendingRemitRequests->sum('total_fees');
//        $pendingRemitRequests = $pendingRemitRequests->orderBy('created_at', 'desc')->paginate(100);
//        if($request->all()){
//            $pendingRemitRequests = $remitRequests::where(function($query) use ($request){
//                if($request->remit_status !== 'all'){
//                    $query->where('sent', (int)$request->remit_status);
//                }
//            })
//                ->where(function ($query) use ($request) {
//                    if($request->remit_amount !== 'all'){
//                        $query->orWhere('remitamt',  $request->remit_amount);
//                    }
//                })
//                ->where(function ($query) use ($request) {
//                    if($request->rem_name !== null){
//                        $query->orWhere(DB::raw("CONCAT_WS(' ', r_fname, r_mname, r_lname)"), 'LIKE', '%'. $request->rem_name. '%');
//                    }
//                })
//                ->where(function ($query) use ($request) {
//                    if($request->ben_name !== null){
//                        $query->orWhere(DB::raw("CONCAT_WS(' ', b_fname, b_mname, b_lname)"), 'LIKE', '%'. $request->ben_name. '%');
//                    }
//                })
//                ->where(function ($query) use ($request) {
//                    if($request->start_date != null){
//                        $start_date =  date('Y-m-d', strtotime($request->start_date));
//                        $query->whereRaw('date(created_at) >= ?', [$start_date]);
//                    }
//                    if($request->end_date){
//                        $end_date =  date('Y-m-d', strtotime($request->end_date));
//                        $query->whereRaw('date(created_at) <= ?', [$end_date]);
//                    }
//                });
//            $totalRemitAmountUsd = $pendingRemitRequests->sum('remitamt');
//            $totalRemitFees = $pendingRemitRequests->sum('total_fees');
//            $pendingRemitRequests = $pendingRemitRequests->orderBy('created_at', 'desc')->paginate(100);
//        }
//        return view('settings.requests.remit', compact('totalRemitRequests', 'totalPendingRemitRequests', 'pendingRemitRequests', 'totalRemitAmountUsd', 'totalRemitFees'));
//    }

}