<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 6/25/2018
 * Time: 4:34 PM
 */

namespace App\Http\Controllers\Dashboard;


use App\Http\Business_Logic\CurrencyBL;
use App\Http\Business_Logic\ProductsBL;
use App\Http\Business_Logic\Traits\LogsTrait;
use App\Http\Business_Logic\UnitOfMeasurement\UnitOfMeasurementTypeBL;
use App\Http\Business_Logic\UnitOfMeasurement\UnitsOfMeasurementBL;
use App\Http\Business_Logic\UserBL;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SettingsCont extends BaseCont
{
    use LogsTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function showCurrencySettings()
    {
        $oCurrencyBL = new CurrencyBL();
        $oCurrencies = $oCurrencyBL->getAllCurrencies();
        return view('dashboard.settings.currency_settings', compact('oCurrencies'));
    }

    public function processCreateCurrency(Request $oRequest)
    {
        $oRequest->validate([
            'name' => 'required|unique:currencies,name',
            'abbr' => 'required|unique:currencies,abbr',
            'symbol' => 'required|unique:currencies,symbol',
        ]);
        $oCurrencyBL = new CurrencyBL();
        $oCurrencyBL->createCurrency($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Created currency');
        return redirect()->route('showCurrencySettings');
    }

    public function processUpdateCurrency(Request $oRequest)
    {
        $oRequest->validate([
            'id'   => 'required|exists:currencies,id',
            'name' => 'required',
            'abbr' => 'required',
            'symbol' => 'required',
        ]);
        $oCurrencyBL = new CurrencyBL();
        $oCurrencyBL->updateCurrency($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Updated currency');
        return redirect()->route('showCurrencySettings');
    }

    public function processDeleteCurrency(Request $oRequest)
    {
        $oRequest->validate([
            'id'   => 'required|exists:currencies,id',
        ]);
        $oCurrencyBL = new CurrencyBL();
        $oCurrencyBL->deleteCurrency($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Deleted currency');
        return redirect()->route('showCurrencySettings');
    }

    public function showProductSettingsCategories()
    {
        $oProductsBL = new ProductsBL();
        $aProductCategories = $oProductsBL->getAllProductCategories();
        return view('dashboard.settings.product_categories', compact('aProductCategories'));
    }

    public function processCreateProductCategory(Request $oRequest)
    {
        $oRequest->validate([
            'name' => 'required|unique:product_categories,name',
            'parent_category_id' => 'nullable|exists:product_categories,id'
        ]);
        $oProductsBL = new ProductsBL();
        $oProductsBL->createProductCategory($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Created product category');
        return redirect()->route('showProductSettingsCategories');
    }

    public function processUpdateProductCategory(Request $oRequest)
    {
        $oRequest->validate([
            'id' => 'required|exists:product_categories,id',
            'name' => 'required',
            'parent_category_id' => 'nullable|exists:product_categories,id'
        ]);
        $oProductsBL = new ProductsBL();
        $oProductsBL->updateProductCategory($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Updated product category');
        return redirect()->route('showProductSettingsCategories');
    }

    public function processDeleteProductCategory(Request $oRequest)
    {
        $oRequest->validate([
            'id' => 'required|exists:product_categories,id',
        ]);
        $oProductsBL = new ProductsBL();
        $oProductsBL->deleteProductCategory($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Deleted product category');
        return redirect()->route('showProductSettingsCategories');
    }

    public function showSelectUnitOfMeasurementType()
    {
        return view('dashboard.settings.unit_of_measurement_type_select');
    }

    public function processSelectUnitOfMeasurementType(Request $oRequest)
    {
        $oRequest->validate([
            'unit_of_measurement_type' => [
                'required',
                Rule::in(['metric', 'imperial']),
            ]
        ]);
        $oUnitOfMeasurementTypeBL = new UnitOfMeasurementTypeBL();
        $aProcessResults = $oUnitOfMeasurementTypeBL->create($oRequest->unit_of_measurement_type);

        if($aProcessResults['status'] === 'success')
        {
            $oUnitsOfMeasurementBL = new UnitsOfMeasurementBL();
            $oUnitsOfMeasurementBL->setActiveMultiple($oRequest->unit_of_measurement_type);
        }

        LogsTrait::create(auth()->user()->name, 'Unit of measurement type selected');
        return redirect()->route('showUnitsOfMeasurement')->with($aProcessResults['status'], $aProcessResults['messages']);
    }

    public function showUnitsOfMeasurement()
    {
        $oUnitsOfMeasurementTypeBL = new UnitOfMeasurementTypeBL();
        $oUnitOfMeasurementType = $oUnitsOfMeasurementTypeBL->getFirst();
        if($oUnitOfMeasurementType['status'] === 'danger'){
            return redirect()->route('showSelectUnitOfMeasurementType');
        }
        $oUnitsOfMeasurementBL = new UnitsOfMeasurementBL();
        $oUnitsOfMeasurement = $oUnitsOfMeasurementBL->getAll();
        return view('dashboard.settings.units_of_measurement', compact('oUnitsOfMeasurement', 'oUnitOfMeasurementType'));
    }

    public function processCreateUnitOfMeasurement(Request $oRequest)
    {
        $oRequest->validate([
            'name' => 'required|unique:units_of_measurement,name',
        ]);
        $oUnitsOfMeasurementBL = new UnitsOfMeasurementBL();
        $oUnitsOfMeasurementBL->createUnitOfMeasurement($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Created unit of measurement');
        return redirect()->route('showUnitsOfMeasurement');
    }

    public function processUpdateUnitOfMeasurement(Request $oRequest)
    {
        $oRequest->validate([
            'id' => 'required|exists:units_of_measurement,id',
            'name' => 'required'
        ]);
        $oUnitsOfMeasurementBL = new UnitsOfMeasurementBL();
        $oUnitsOfMeasurementBL->updateUnitOfMeasurement($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Updated unit of measurement');
        return redirect()->route('showUnitsOfMeasurement');
    }

    public function processDeleteUnitOfMeasurement(Request $oRequest)
    {
        $oRequest->validate([
            'id' => 'required|exists:units_of_measurement,id',
        ]);
        $oUnitsOfMeasurementBL = new UnitsOfMeasurementBL();
        $oUnitsOfMeasurementBL->deleteUnitOfMeasurement($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Deleted unit of measurement');
        return redirect()->route('showUnitsOfMeasurement');
    }

    public function showUserSettings()
    {
        $oUserBL = new UserBL();
        $oUsers = $oUserBL->getAll();
        return view('dashboard.settings.user_settings', compact('oUsers'));
    }

    public function processCreateUser(Request $oRequest)
    {
        $oRequest->validate([
            'username' => 'required|unique:users,username',
            'name' => 'required',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);
        $oUsersBL = new UserBL();
        $aProcessResults = $oUsersBL->create($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Created new user');
        return redirect()->route('showUserSettings')->with($aProcessResults['status'], $aProcessResults['messages']);
    }

    public function processEditUser(Request $oRequest)
    {
        $oRequest->validate([
            'id' => 'required|exists:users,id',
            'username' => 'required|unique:users,username',
            'name' => 'required',
        ]);
        $oUsersBL = new UserBL();
        $aProcessResults = $oUsersBL->update($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Edited user');
        return redirect()->route('showUserSettings')->with($aProcessResults['status'], $aProcessResults['messages']);
    }

    public function processDeleteUser(Request $oRequest)
    {
        $oRequest->validate([
            'id' => 'required|exists:users,id',
        ]);
        $oUsersBL = new UserBL();
        $aProcessResults = $oUsersBL->delete($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Deleted user');
        return redirect()->route('showUserSettings')->with($aProcessResults['status'], $aProcessResults['messages']);
    }

    public function processActivateCurrency(Request $oRequest)
    {
        $oRequest->validate([
            'id' => 'required|exists:currencies,id'
        ]);
        $oCurrencyBL = new CurrencyBL();
        $oCurrencyBL->activateCurrency($oRequest->id);
        LogsTrait::create(auth()->user()->name, 'Activated currency status');
        return redirect()->route('showCurrencySettings');
    }
}