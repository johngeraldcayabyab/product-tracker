<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 6/6/2018
 * Time: 9:04 PM
 */

namespace App\Http\Controllers\Dashboard;


use App\Http\Business_Logic\BillOfMaterialsBL;
use App\Http\Business_Logic\ProductsBL;
use App\Http\Business_Logic\Traits\LogsTrait;
use App\Http\Business_Logic\UnitOfMeasurement\UnitsOfMeasurementBL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class InventoryCont extends BaseCont
{
    use LogsTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function showProducts()
    {
        $oProductsBL = new ProductsBL();
        $oDatas   = $oProductsBL->getAllProducts();
        $oProductCategories = $oProductsBL->getAllProductCategories();
        return view('dashboard/inventory/products', compact('oDatas', 'oProductCategories'));
    }

    public function showCreateProducts($iId)
    {
        $oProductsBL           = new ProductsBL();
        $oProductCategories    = $oProductsBL->getAllProductCategories();
        $oUnitsOfMeasurementBL = new UnitsOfMeasurementBL();
        $oMeasurements         = $oUnitsOfMeasurementBL->getAllActive();
        if((int)$iId === 0)
        {
            return view('dashboard/inventory/products_create', compact('oMeasurements', 'oProductCategories'));
        }
        $aProcessResults = $oProductsBL->getProductById($iId);
        if($aProcessResults['status'] === 'success')
        {
            $oBillOfMaterialsModel = new BillOfMaterialsBL();
            $oBoms                 = $oBillOfMaterialsModel->getBoms($iId)['data'];
            $oProduct              = $aProcessResults['data'];
            return view('dashboard/inventory/products_create', compact('oMeasurements', 'oProductCategories', 'oProduct', 'oBoms'));
        }
        return redirect()->route('showProducts')->with($aProcessResults['status'], $aProcessResults['messages']);
    }

    public function processCreateProducts(Request $oRequest)
    {
        $aRules =
        [
            'name'             => 'required|unique:products,name',
            'quantity_on_hand' => 'nullable|between:0,99.99',
            'sales_price'      => 'nullable|between:0,99.99',
            'cost_price'       => 'nullable|between:0,99.99',
            'product_type'     => ['nullable', Rule::in(['Stockable', 'Consumable', 'Service'])],
            'product_category' => 'nullable|exists:product_categories,id',
            'measurement_type' => 'nullable|exists:units_of_measurement,id',
            'upc'              => 'nullable|max:255',
            'sku'              => 'nullable|max:255',
            'bom_id.*'         => 'nullable|exists:products,id',
            'bom_quantity.*'   => 'nullable',
            'image_name'       => 'nullable|mimes:png,jpg,jpeg,gif,bmp'
        ];
        if($oRequest->bom_id !== null && array_filter($oRequest->bom_id))
        {
            $aRules['bom_quantity.*'] = 'required|integer';
        }
        $aValidationResults = $oRequest->validate($aRules);
        if ($oRequest->hasFile('image_name')) {
            $oRequest->image_name->storeAs('public/images/products/', strtolower(str_replace(' ', '_', $oRequest->name) . '.png'));
        }
        $oProductsBL = new ProductsBL();
        $aProcessResults = $oProductsBL->createProducts($aValidationResults);
        if($aProcessResults['status'] === 'success' && $oRequest->bom_id !== null && array_filter($oRequest->bom_id))
        {
            $oBillOfMaterialsBL = new BillOfMaterialsBL();
            $oBillOfMaterialsBL->createBoms($aProcessResults['data']->id, $aValidationResults['bom_id'], $aValidationResults['bom_quantity']);
        }
        LogsTrait::create(auth()->user()->name, 'Created ' . $aProcessResults['data']->name . ' product');
        return redirect()->route('showCreateProducts', ['iId' => $aProcessResults['data']->id])->with($aProcessResults['status'], $aProcessResults['messages']);
    }


    public function processEditProducts(Request $oRequest)
    {
        $aRules =
        [
            'id'               => 'required|exists:products,id',
            'name'             => 'required',
            'quantity_on_hand' => 'nullable|between:0,99.99',
            'sales_price'      => 'nullable|between:0,99.99',
            'cost_price'       => 'nullable|between:0,99.99',
            'product_type'     => ['nullable', Rule::in(['Stockable', 'Consumable', 'Service'])],
            'product_category' => 'nullable|exists:product_categories,id',
            'measurement_type' => 'nullable|exists:units_of_measurement,id',
            'upc'              => 'nullable|max:255',
            'sku'              => 'nullable|max:255',
            'bom_id.*'         => 'nullable|exists:products,id',
            'bom_quantity.*'   => 'nullable',
            'image_name'       => 'nullable|mimes:png,jpg,jpeg,gif,bmp'
        ];
        if($oRequest->bom_id !== null && array_filter($oRequest->bom_id)) {
            $aRules['bom_quantity.*'] = 'required|integer';
        }
        $aValidationResults = $oRequest->validate($aRules);
        if ($oRequest->hasFile('image_name')) {
            $oRequest->image_name->storeAs('public/images/products/', strtolower(str_replace(' ', '_', $oRequest->name) . '.png'));
        }
        $oProductsBL = new ProductsBL();
        $aProcessResults = $oProductsBL->editProducts($aValidationResults);
        if($aProcessResults['status'] === 'success' && $oRequest->bom_id !== null && array_filter($oRequest->bom_id))
        {
            $oBillOfMaterialsBL = new BillOfMaterialsBL();
            $oBillOfMaterialsBL->delete($aProcessResults['data']->id);
            $oBillOfMaterialsBL = new BillOfMaterialsBL();
            $oBillOfMaterialsBL->createBoms($aProcessResults['data']->id, $aValidationResults['bom_id'], $aValidationResults['bom_quantity']);
        }
        LogsTrait::create(auth()->user()->name, 'Edited ' . $aProcessResults['data']->name . ' product');
        return redirect()->route('showCreateProducts', ['iId' => $aValidationResults['id']])->with($aProcessResults['status'], $aProcessResults['messages']);
    }

    public function processDeleteProducts(Request $oRequest)
    {
        $aValidationResults = $oRequest->validate(['id' => 'required|exists:products,id']);
        $oProductsBL = new ProductsBL();
        $aProcessResults = $oProductsBL->deleteProducts($aValidationResults);
        if($aProcessResults['status'] === 'success'){
            $oBillOfMaterialsBL = new BillOfMaterialsBL();
            $oBillOfMaterialsBL->delete($aProcessResults['data']->id);
        }
        if($aProcessResults['data']->image_name)
        {
            $sImageLocation = 'public/images/products/' . $aProcessResults['data']->image_name;
            Storage::delete($sImageLocation);
        }
        LogsTrait::create(auth()->user()->name, 'Deleted ' . $aProcessResults['data']->name . ' product');
        return redirect()->route('showProducts')->with($aProcessResults['status'], $aProcessResults['messages']);
    }

}