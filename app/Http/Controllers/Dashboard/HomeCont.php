<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 6/6/2018
 * Time: 9:06 PM
 */

namespace App\Http\Controllers\Dashboard;


use App\Http\Business_Logic\ProductsBL;
use App\Http\Business_Logic\Traits\LogsTrait;
use App\Http\Business_Logic\UserBL;
use Illuminate\Http\Request;

class HomeCont extends BaseCont
{
    use LogsTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function showHome()
    {
        $oUserBL = new UserBL();
        $oProductsBL = new ProductsBL();
        $iNumberOfUsers = $oUserBL->countAllUsers();
        $iNumberOfProducts = $oProductsBL->countNumberOfProducts();
        $oLogs = LogsTrait::getAll();
        return view('dashboard.home.company_info', compact('iNumberOfUsers', 'iNumberOfProducts', 'oLogs'));
    }

    public function processUpdateCompanyLogo(Request $oRequest)
    {
        $oRequest->validate([
            'company_logo' => 'required'
        ]);

        if ($oRequest->hasFile('company_logo')) {
            $oRequest->company_logo->storeAs('public/images/logo', 'company_logo.png');
        }
        LogsTrait::create(auth()->user()->name, 'Updated company logo');
        return redirect()->route('showHome');
    }
}