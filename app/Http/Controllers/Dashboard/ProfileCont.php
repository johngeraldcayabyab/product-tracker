<?php
/**
 * Created by John Gerald B. Cayabyab. | johngeraldcayabyab@gmail.com
 * Date: 14/08/2018
 * Time: 8:37 AM
 */

namespace App\Http\Controllers\Dashboard;



use App\Http\Business_Logic\Traits\LogsTrait;
use App\Http\Business_Logic\UserBL;
use Illuminate\Http\Request;

class ProfileCont extends BaseCont
{
    use LogsTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function showUserProfile()
    {
        $oUsersBL = new UserBL();
        $oUser = $oUsersBL->findUserById(auth()->user()->id);
        return view('dashboard.profile.profile', compact('oUser'));
    }

    public function processUpdateProfile(Request $oRequest)
    {
        $oRequest->validate([
            'id' => 'required|exists:users,id',
            'username' => 'unique:users,username,' . $oRequest->id . '|required',
            'name' => 'required'
        ]);

        $oUserBL = new UserBL();
        $aProcessResults = $oUserBL->updateProfile($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Updated profile');
        return redirect()->route('showUserProfile')->with($aProcessResults['status'], $aProcessResults['messages']);
    }

    public function processUpdatePassword(Request $oRequest)
    {
        $oRequest->validate([
            'current_password' => 'required',
            'new_password' => 'required',
            'confirm_new_password' => 'required|same:new_password'
        ]);
        $oUserBL = new UserBL();
        $aProcessResults = $oUserBL->updatePassword($oRequest->all());
        LogsTrait::create(auth()->user()->name, 'Changed password');
        return redirect()->route('showUserProfile')->with($aProcessResults['status'], $aProcessResults['messages']);
    }

}