<?php

/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 5/14/2018
 * Time: 7:21 PM
 */

namespace App\Http\Controllers\Dashboard;

use App\Http\Business_Logic\CurrencyBL;
use App\Http\Business_Logic\UnitOfMeasurement\UnitsOfMeasurementBL;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class BaseCont extends Controller
{

    public function __construct()
    {
        View::share ( 'aSideNavLinks', $this->sideNavLinks() );
        View::share ( 'companyLogo', $this->companyLogo() );
        View::share ('oUnitsOfMeasurements', $this->unitsOfMeasurement());
        View::share ('oCurrency', $this->activeCurrency());
    }

    public function activeCurrency()
    {
        $oCurrencyBL = new CurrencyBL();
        return $oCurrencyBL->getActiveCurrency();
    }

    public function unitsOfMeasurement()
    {
        $oUnitsOfMeasurementBL = new UnitsOfMeasurementBL();
        return $oUnitsOfMeasurementBL->getAllActive();
    }

    public function companyLogo()
    {
        $companyLogo = asset('img/logos/company_logo.png');
        try
        {
            if(Storage::get('public/images/logo/company_logo.png')){
                $companyLogo = asset('storage/images/logo/company_logo.png');
            };
        }catch(\Exception $e)
        {

        }
        return $companyLogo;
    }

    public function sideNavLinks()
    {
        return
        [
            [
                'sub_links' =>
                [
                    'Company' => route('showHome')
                ],
                'id' => 'home',
                'link' => 'Home',
                'base_url' => '/dashboard/home',
                'icon' => 'home'
            ],

            [
                'sub_links' =>
                [
                    'Product categories' => route('showProductSettingsCategories'),
                    'Units of measurement' => route('showUnitsOfMeasurement'),
                    'Users' => route('showUserSettings'),
                    'Currency settings' => route('showCurrencySettings')
                ],
                'id' => 'settings',
                'link' => 'Settings',
                'base_url' => '/dashboard/settings',
                'icon' => 'cogs'
            ],

            [
                'sub_links' =>
                [
                    'Products' => route('showProducts'),
                ],
                'id' => 'inventory',
                'link' => 'Inventory',
                'base_url' => '/dashboard/inventory',
                'icon' => 'box-open'
            ],



        ];
    }
}