<?php
/**
 * Created by John Gerald B. Cayabyab. | johngeraldcayabyab@gmail.com
 * Date: 25/07/2018
 * Time: 11:55 AM
 */

namespace App\Http\Controllers\Api;


use App\Http\Business_Logic\ProductsBL;
use Illuminate\Http\Request;

class ProductsApi extends ProductsBL
{

    public function apiGetProducts(Request $oRequest)
    {
        return parent::getProducts($oRequest);
    }

    public function apiGetNamesAndQuantity(Request $oRequest)
    {
        $oProductNamesAndQuantityJson = ['suggestions' => []];
        $oProductNamesAndQuantity = parent::searchNamesPlusQuantity($oRequest->sKeyword);
        foreach($oProductNamesAndQuantity as $oProductNameAndQuantity)
        {
            $oProductNamesAndQuantityJson['suggestions'][] = ['value' => $oProductNameAndQuantity->name, 'data' => $oProductNameAndQuantity];
        }
        return $oProductNamesAndQuantityJson;
    }


}