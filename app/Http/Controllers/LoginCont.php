<?php

/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 5/16/2018
 * Time: 10:32 PM
 */

namespace App\Http\Controllers;

use App\Http\Business_Logic\Traits\LogsTrait;
use App\Http\Business_Logic\UserBL;
use App\Http\Controllers\Dashboard\BaseCont;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginCont extends BaseCont
{
    use LogsTrait;

    private $oUserBl;

    public function __construct(UserBL $oUserBl)
    {
        parent::__construct();
        $this->oUserBl = $oUserBl;
    }

    public function index()
    {
        if(Auth::check()){
            return redirect()->route('showHome');
        }
        return view('login.login');
    }

    public function loginProcess(Request $oRequest)
    {
        if(Auth::check())
        {
            return redirect()->route('showHome');
        }

        $aCredentials = $oRequest->validate([
            'username' => 'required|exists:users,username',
            'password' => 'required'
        ]);

        if(Auth::attempt($aCredentials))
        {
            $bResult = $this->oUserBl->verifyPassword($aCredentials['username'], $aCredentials['password']);
            if($bResult)
            {
                LogsTrait::create(auth()->user()->name, 'Logged in');
                return redirect()->route('showHome');
            }
            return redirect()->route('login');
        }

        return redirect()->route('login');
    }
}