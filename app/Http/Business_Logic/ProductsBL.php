<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 6/6/2018
 * Time: 12:41 AM
 */

namespace App\Http\Business_Logic;


use App\Http\Models\ProductCategoriesModel;
use App\Http\Models\ProductsModel;

class ProductsBL
{

    public function getProductById($iId)
    {
        $aResults =
            [
                'status' => 'success',
                'messages' => [],
                'data' => null,
            ];
        try
        {
            $oProduct = ProductsModel::find($iId);
            $aResults['data'] = $oProduct;
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = 'Product does not exist!';
            $aResults['data'] = $e->getMessage();
        };
        return $aResults;
    }



    /**
     * This is for the reference 'code' in the BOM
     * table
     */
    const RAW = 'RAW';

    public function getProducts($aData)
    {
        $aResults =
        [
            'status' => 'success',
            'messages' => [],
            'data' => null,
        ];
        try
        {
            if($aData)
            {
                $oProducts = ProductsModel::with('measurementType')->where(function($query) use ($aData){
                    if($aData['product_category'] !== null){
                        $query->where('product_category', $aData['product_category']);
                    }
                })
                ->where(function($query) use ($aData){
                    if($aData['product_type'] !== null){
                        $query->where('product_type', $aData['product_type']);
                    }
                })
                ->where(function($query) use ($aData){
                    if($aData['name'] !== null){
                        $query->where('name', 'like', '%' . $aData['name'] . '%');
                    }
                })
                ->where(function($query) use ($aData){
                    if($aData['measurement_type'] !== null){
                        $query->where('measurement_type', $aData['measurement_type']);
                    }
                });
                $iLimit = isset($aData['limit']) ? $aData['limit'] : null;
                $aResults['data'] = $oProducts->orderBy('name', 'asc')->paginate($iLimit);
            }
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        }
        return $aResults;
    }

    public function searchNamesPlusQuantity($sKeyword)
    {
        $oProductsModel = new ProductsModel();
        $oProducts = $oProductsModel::select('id', 'name', 'quantity_on_hand', 'product_type');
        if($sKeyword) {
            $oProducts = $oProductsModel::where(function ($query) use ($sKeyword) {
                if ($sKeyword !== null) {
                    $query->where('name', 'like', '%' . $sKeyword . '%');
                }
            });
        }
        return $oProducts->orderBy('name', 'asc')->get();
    }


    public function getAllProducts()
    {
        $oProductsModel = new ProductsModel();
        return $oProductsModel::with('measurementType')->orderBy('name')->paginate(100);
    }


    public function createProducts($aData)
    {
        $aResults =
        [
            'status' => 'success',
            'messages' => [],
            'data' => null,
        ];
        try
        {
            $oProductsModel = new ProductsModel();
            $oProductsModel->name = $aData['name'];
            $oProductsModel->sales_price = isset($aData['sales_price']) ? $aData['sales_price'] : null;
            $oProductsModel->cost_price = isset($aData['cost_price']) ? $aData['cost_price'] : null;
            $oProductsModel->product_type = isset($aData['product_type']) ? $aData['product_type'] : null;
            $oProductsModel->product_category = isset($aData['product_category']) ? $aData['product_category'] : null;
            $oProductsModel->measurement_type = isset($aData['measurement_type']) ? $aData['measurement_type'] : null;
            $oProductsModel->quantity_on_hand = isset($aData['quantity_on_hand']) ? $aData['quantity_on_hand'] : null;
            $oProductsModel->image_name = isset($aData['image_name']) ? strtolower(str_replace(' ', '_', $aData['name']) . '.png') : null;
            $oProductsModel->sku = isset($aData['sku']) ? $aData['sku'] : null;
            $oProductsModel->upc = isset($aData['upc']) ? $aData['upc'] : null;
            $oProductsModel->save();
            $aResults['messages'][] = 'Product ' . $oProductsModel->name . ' has been created successfully!';
            $aResults['data'] = $oProductsModel;
        }catch (\Exception $e){
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        }
        return $aResults;
    }

    public function editProducts($aData)
    {
        $aResults =
        [
            'status' => 'success',
            'messages' => [],
            'data' => null,
        ];
        try
        {
            $oProductsModel = ProductsModel::find($aData['id']);
            $oProductsModel->name = $aData['name'];
            $oProductsModel->sales_price = isset($aData['sales_price']) ? $aData['sales_price'] : null;
            $oProductsModel->cost_price = isset($aData['cost_price']) ? $aData['cost_price'] : null;
            $oProductsModel->product_type = isset($aData['product_type']) ? $aData['product_type'] : null;
            $oProductsModel->product_category = isset($aData['product_category']) ? $aData['product_category'] : null;
            $oProductsModel->measurement_type = isset($aData['measurement_type']) ? $aData['measurement_type'] : null;
            $oProductsModel->quantity_on_hand = isset($aData['quantity_on_hand']) ? $aData['quantity_on_hand'] : null;
            $oProductsModel->image_name = isset($aData['image_name']) ? strtolower(str_replace(' ', '_', $aData['name']) . '.png') : null;
            $oProductsModel->sku = isset($aData['sku']) ? $aData['sku'] : null;
            $oProductsModel->upc = isset($aData['upc']) ? $aData['upc'] : null;
            $oProductsModel->save();
            $aResults['messages'][] = 'Product ' . $oProductsModel->name . ' edited successfully!';
            $aResults['data'] = $oProductsModel;
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        }
        return $aResults;
    }

    public function deleteProducts($aData)
    {
        $aResults =
        [
            'status' => 'success',
            'messages' => [],
            'data' => null,
        ];
        try
        {
            $oProductsModel = ProductsModel::find($aData['id']);
            $oProductsModel->delete();
            $aResults['messages'][] = 'Product ' . $oProductsModel->name . ' has been deleted successfully!';
            $aResults['data'] = $oProductsModel;
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        };
        return $aResults;
    }

    public function getParentCategory($array)
    {
        $result = array();
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $result = array_merge($result, $this->getParentCategory($value));
            }
            else
            {
                if($key === 'name'){
                    $result[] = $value;
                }
            }
        };
        return $result;
    }

    public function appendSameKeys($aData)
    {
        $aData = array_reverse($aData);
        array_pop($aData);
        $sParentCategory = '';
        foreach($aData as $sData){
            $sParentCategory .= $sData . ' / ';
        }
        return substr($sParentCategory, 0, -3);
    }

    public function getAllProductCategories()
    {
        $aProductCategories = ProductCategoriesModel::with('parentRecursive')->get()->toArray();
        foreach($aProductCategories as $iKey => $aProductCategory){
            $aProductCategories[$iKey]['theParent'] =  $this->appendSameKeys($this->getParentCategory($aProductCategory));
        }
        return $aProductCategories;
    }

    public function createProductCategory($aData)
    {
        $oProductCategoriesModel = new ProductCategoriesModel();
        $iProductCategory = null;
        $oProductCategoriesModel->name = $aData['name'];
        $oProductCategoriesModel->parent_category_id = array_key_exists('parent_category_id', $aData) ? $aData['parent_category_id'] : null;
        $oProductCategoriesModel->save();
        return $oProductCategoriesModel;
    }

    public function updateProductCategory($aData)
    {
        $oProductCategoriesModel = ProductCategoriesModel::find($aData['id']);
        $iParentCategoryId = null;
        $oProductCategoriesModel->name = $aData['name'];
        $oProductCategoriesModel->parent_category_id = array_key_exists('parent_category_id', $aData) ? $aData['parent_category_id'] : null;
        $oProductCategoriesModel->save();
        return $oProductCategoriesModel;
    }

    public function deleteProductCategory($aData)
    {
        $oProductCategoriesModel = ProductCategoriesModel::find($aData['id']);
        $oProductCategoriesModel->delete();
    }


    public function addQuantity($sName, $iQuantity)
    {
        $oProductsModel = ProductsModel::where('name', $sName)->get()->first();
        $oProductsModel->quantity = $oProductsModel->quantity + $iQuantity;
        $oProductsModel->save();
        return true;
    }

    public function countNumberOfProducts()
    {
        return ProductsModel::count();
    }

}