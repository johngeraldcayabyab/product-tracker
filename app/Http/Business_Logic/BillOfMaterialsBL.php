<?php
/**
 * Created by John Gerald B. Cayabyab. | johngeraldcayabyab@gmail.com
 * Date: 26/07/2018
 * Time: 11:08 AM
 */

namespace App\Http\Business_Logic;


use App\Http\Models\BillOfMaterialsModel;

class BillOfMaterialsBL
{
    const BOM = 'BOM';

    public function createBoms($iProductId, $aProductIds, $aQuantities)
    {
        $aResults =
        [
            'status' => 'success',
            'messages' => [],
            'data' => null,
        ];
        foreach($aProductIds as $iIndex => $aProductId)
        {
            try
            {
                $oBillOfMaterialsModel = new BillOfMaterialsModel();
                $oBillOfMaterialsModel->reference_id = self::BOM . $iProductId;
                $oBillOfMaterialsModel->product_id = $aProductId;
                $oBillOfMaterialsModel->quantity = $aQuantities[$iIndex];
                $oBillOfMaterialsModel->save();
                $aResults['messages'][] = 'Bill of material ' . $oBillOfMaterialsModel->id . ' has been created successfully!';
            }
            catch(\Exception $e)
            {
                $aResults['status'] = 'danger';
                $aResults['messages'][] = $e->getMessage();
                $aResults['data'] = $e->getMessage();
            }
        }
        return $aResults;
    }



    public function getBoms($iProductId)
    {
        $aResults =
        [
            'status' => 'success',
            'messages' => [],
            'data' => null,
        ];
        try
        {
            $aResults['data'] = BillOfMaterialsModel::with('products')->where('reference_id', self::BOM . $iProductId)->get();
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        }
        return $aResults;
    }

    public function delete($iProductId)
    {
        $aResults =
            [
                'status' => 'success',
                'messages' => [],
                'data' => null,
            ];
        try
        {
            $aResults['data'] = BillOfMaterialsModel::where('reference_id', self::BOM . $iProductId)->delete();
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        }
        return $aResults;
    }


}