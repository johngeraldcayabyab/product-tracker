<?php
namespace App\Http\Business_Logic;

use App\Http\Models\UsersModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 5/22/2018
 * Time: 12:30 AM
 */
class UserBL
{
    private $oUserModel;

    public function __construct()
    {
        $this->oUserModel = new UsersModel();
    }

    public function getAll()
    {
        return $this->oUserModel->paginate(20);
    }

    public function countAllUsers()
    {
        return $this->oUserModel->count();
    }

    public function create($aData)
    {
        $aResults =
            [
                'status' => 'success',
                'messages' => [],
                'data' => null,
            ];
        try
        {
            $oUsersModel = new UsersModel();
            $oUsersModel->username = $aData['username'];
            $oUsersModel->password = Hash::make($aData['password']);
            $oUsersModel->name = $aData['name'];
            $oUsersModel->save();
            $aResults['messages'][] = 'User ' . $oUsersModel->name . ' has been created successfully!';
            $aResults['data'] = $oUsersModel;
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        };
        return $aResults;
    }

    public function update($aData)
    {
        $aResults =
            [
                'status' => 'success',
                'messages' => [],
                'data' => null,
            ];
        try
        {
            $oUsersModel = UsersModel::find($aData['id']);
            $oUsersModel->username = $aData['username'];
            $oUsersModel->name = $aData['name'];
            $oUsersModel->save();
            $aResults['messages'][] = 'User ' . $oUsersModel->name . ' has been edit successfully!';
            $aResults['data'] = $oUsersModel;
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        };
        return $aResults;
    }

    public function delete($aData)
    {
        $aResults =
            [
                'status' => 'success',
                'messages' => [],
                'data' => null,
            ];
        try
        {
            $oUsersModel = UsersModel::find($aData['id']);
            $oUsersModel->delete();
            $aResults['messages'][] = 'User ' . $oUsersModel->name . ' has been deleted successfully!';
            $aResults['data'] = $oUsersModel;
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        };
        return $aResults;
    }

    public function verifyPassword($sUsername, $sPassword)
    {
        $aResult = $this->oUserModel->where('username', $sUsername)->first();
        if (Hash::check($sPassword, $aResult->password))
        {
            return true;
        }
        return false;
    }

    public function updateProfile($aData)
    {
        $aResults =
        [
            'status' => 'success',
            'messages' => [],
            'data' => null,
        ];
        try
        {
            $oUsersModel = UsersModel::find($aData['id']);
            $oUsersModel->username = $aData['username'];
            $oUsersModel->name = $aData['name'];
            $oUsersModel->save();
            $aResults['messages'][] = 'Profile has been updated successfully!';
            $aResults['data'] = $oUsersModel;
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        };
        return $aResults;
    }

    public function findUserById($iId)
    {
        return $this->oUserModel::find($iId);
    }

    public function updatePassword($aData)
    {
        $aResults =
            [
                'status' => 'success',
                'messages' => [],
                'data' => null,
            ];
        try
        {
            $oUser = UsersModel::find($aData['id']);
            if(!Hash::check($aData['current_password'], $oUser->password))
            {
                $aResults['status'] = 'danger';
                $aResults['messages'][] = 'Current password is incorrect';
                $aResults['data'] = $oUser;
            }
            else
            {
                $oUser->password = Hash::make($aData['new_password']);
                $oUser->save();
                $aResults['messages'][] = 'Password has been updated successfully!';
                $aResults['data'] = $oUser;
            }
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        };
        return $aResults;
    }
}