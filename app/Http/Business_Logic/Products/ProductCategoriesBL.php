<?php
/**
 * Created by John Gerald B. Cayabyab. | johngeraldcayabyab@gmail.com
 * Date: 31/07/2018
 * Time: 8:28 AM
 */

namespace App\Http\Business_Logic\Products;


use App\Http\Models\ProductCategoriesModel;

class ProductCategoriesBL
{
    /**
     * @return array
     */
    public function getAllProductCategories()
    {
        $aProductCategories = ProductCategoriesModel::with('parentRecursive')->get()->toArray();
        foreach($aProductCategories as $iKey => $aProductCategory){
            $aProductCategories[$iKey]['parent'] =  $this->appendSameKeys($this->getParentCategory($aProductCategory));
        }
        return $aProductCategories;
    }

    /**
     * @param $aData
     * @return bool|string
     */
    private function appendSameKeys($aData)
    {
        $aData = array_reverse($aData);
        array_pop($aData);
        $sParentCategory = '';
        foreach($aData as $sData){
            $sParentCategory .= $sData . ' / ';
        }
        return substr($sParentCategory, 0, -3);
    }

    /**
     * @param $array
     * @return array
     */
    private function getParentCategory($array)
    {
        $result = array();
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $result = array_merge($result, $this->getParentCategory($value));
            }
            else
            {
                if($key === 'name'){
                    $result[] = $value;
                }
            }
        };
        return $result;
    }
}