<?php
/**
 * Created by John Gerald B. Cayabyab. | johngeraldcayabyab@gmail.com
 * Date: 15/08/2018
 * Time: 10:34 AM
 */

namespace App\Http\Business_Logic\Traits;


use App\Http\Models\LogsModel;

trait LogsTrait
{
    public static function getAll()
    {
        return LogsModel::orderBy('created_at', 'desc')->paginate(10);
    }

    public static function create($sResponsible, $sAction)
    {
        $oLogsModel = new LogsModel();
        $oLogsModel->responsible = $sResponsible;
        $oLogsModel->action = $sAction;
        $oLogsModel->save();
    }
}