<?php
/**
 * Created by John Gerald B. Cayabyab. | johngeraldcayabyab@gmail.com
 * Date: 02/08/2018
 * Time: 9:37 AM
 */

namespace App\Http\Business_Logic\UnitOfMeasurement;


use App\Http\Models\UnitOfMeasurementTypeModel;

class UnitOfMeasurementTypeBL
{
    public function getFirst()
    {
        $aResults =
        [
            'status' => 'success',
            'messages' => [],
            'data' => null,
        ];
        try
        {
            $oUnitOfMeasurementType = UnitOfMeasurementTypeModel::first();
            if($oUnitOfMeasurementType)
            {
                $aResults['data'] = $oUnitOfMeasurementType;
            }
            else
            {
                $aResults['status'] = 'danger';
            }
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        }
        return $aResults;
    }


    public function create($sType)
    {
        $aResults =
        [
            'status' => 'success',
            'messages' => [],
            'data' => null,
        ];
        try
        {
            $oUnitOfMeasurementTypeModel = new UnitOfMeasurementTypeModel();
            $oUnitOfMeasurementTypeModel->type = $sType;
            $oUnitOfMeasurementTypeModel->save();
            $aResults['messages'][] = 'Unit of measurement ' . $oUnitOfMeasurementTypeModel->type . ' system has been set successfully!';
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        }
        return $aResults;
    }
}