<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 7/5/2018
 * Time: 6:37 PM
 */

namespace App\Http\Business_Logic\UnitOfMeasurement;


use App\Http\Models\UnitsOfMeasurementModel;

class UnitsOfMeasurementBL
{
    private $oModel;

    public function __construct()
    {
        $this->oModel = new UnitsOfMeasurementModel();
    }

    public function getAll()
    {
        return UnitsOfMeasurementModel::all();
    }


    public function getAllActive()
    {
        return UnitsOfMeasurementModel::where('active', 1)->get();
    }

    public function createUnitOfMeasurement($aData)
    {
        $oUnitsOfMeasurementModel = new UnitsOfMeasurementModel();
        $oUnitsOfMeasurementModel->name = $aData['name'];
        $oUnitsOfMeasurementModel->save();
        return $oUnitsOfMeasurementModel;
    }

    public function setActiveMultiple($sType)
    {
        $aResults =
            [
                'status' => 'success',
                'messages' => [],
                'data' => null,
            ];
        try
        {
            $aResults['data'] = $this->oModel::where('unit_of_measurement_type', $sType)->update(['active' => true]);
        }
        catch(\Exception $e)
        {
            $aResults['status'] = 'danger';
            $aResults['messages'][] = $e->getMessage();
            $aResults['data'] = $e->getMessage();
        }
        return $aResults;
    }

//    public function updateUnitOfMeasurement($aData)
//    {
//        $oUnitsOfMeasurementModel = UnitsOfMeasurementModel::find($aData['id']);
//        $oUnitsOfMeasurementModel->name = $aData['name'];
//        $oUnitsOfMeasurementModel->save();
//        return $oUnitsOfMeasurementModel;
//    }
//
//    public function deleteUnitOfMeasurement($aData)
//    {
//        $oUnitsOfMeasurementModel = UnitsOfMeasurementModel::find($aData['id']);
//        $oUnitsOfMeasurementModel->delete();
//    }

}