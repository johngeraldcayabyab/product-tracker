<?php
/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 7/1/2018
 * Time: 8:54 PM
 */

namespace App\Http\Business_Logic;


use App\Http\Models\CurrenciesModel;

class CurrencyBL
{
    public function getAllCurrencies()
    {
        return CurrenciesModel::paginate(10);
    }

    public function getActiveCurrency()
    {
        return CurrenciesModel::where('active', 1)->get()->first();
    }

    public function createCurrency($aData)
    {
        $oCurrencyModel = new CurrenciesModel();
        $oCurrencyModel->name = $aData['name'];
        $oCurrencyModel->abbr = $aData['abbr'];
        $oCurrencyModel->symbol = $aData['symbol'];
        return $oCurrencyModel->save();
    }

    public function updateCurrency($aData)
    {
        $oCurrencyModel = CurrenciesModel::find($aData['id']);
        $oCurrencyModel->name = $aData['name'];
        $oCurrencyModel->abbr = $aData['abbr'];
        $oCurrencyModel->symbol = $aData['symbol'];
        return $oCurrencyModel->save();
    }

    public function deleteCurrency($aData)
    {
        $oCurrencyModel = CurrenciesModel::find($aData['id']);
        return $oCurrencyModel->delete();
    }

    public function activateCurrency($iId)
    {
        $bStatus = 1;
        $oCurrency = CurrenciesModel::find($iId);
        $oCurrencyModel = new CurrenciesModel();
        if($oCurrency->active)
        {
            $bStatus = 0;
        }
        $oCurrencyModel::where('active', 1)->update(['active' => 0]);
        if($bStatus === 0){
            $oCurrency->active = 0;
        }else{
            $oCurrency->active = 1;
        }
        $oCurrency->save();
    }
}