/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 5/31/2018
 * Time: 1:15 AM
 */


$( document ).ready(function(){



});

function getCsrfToken()
{
    return $('meta[name="csrf-token"]').attr('content');
}




function showNeutron()
{
    $("#nuotron").css('visibility', 'visible');
}

function hideNeutron()
{
    $("#nuotron").css('visibility', 'hidden');
}


function generateMessage(aMessages, sStatus){
    let errorMessages = '';

    $.each(aMessages, function(key, value){
        errorMessages += value;
    });

    let alertCon = '<div class="alert alert-' + sStatus +' alert-dismissible fade show" role="alert">' +
        errorMessages +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button>' +
        '</div>';

   $('#invisible-alert').append(alertCon);
}