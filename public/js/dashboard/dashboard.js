/**
 * Created by John Gerald B. Cayabyab | johngeraldcayabyab@gmail.com
 * Date: 7/6/2018
 * Time: 1:24 AM
 */

$( document ).ready(function(){

    setSubLinksBulletsColors();


    $('.side-nav-links').click(_.debounce(function(){
        $(this).find('.side-nav-links-icon-status').toggleClass("down");
    }, 50));


});

/**
 * Sets new color for each of sub links bullets from the provided colors
 * Aesthetic purposes
 */
function setSubLinksBulletsColors(){
    let aSubLinksColors = ['#2ecc71', '#3498db', '#9b59b6', '#f1c40f', '#e67e22'];
    let iSubLinksColorsTotal = aSubLinksColors.length;
    let iCounter = 0;
    $( '.sub-nav-sub-links-bullets' ).each(function() {
        if(iCounter === iSubLinksColorsTotal){
            iCounter = 0;
        }
        $(this).css('color', aSubLinksColors[iCounter]);
        iCounter++;
    });
}

