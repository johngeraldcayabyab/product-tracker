// Jquery
window.$ = window.jQuery = require('jquery');

// Lodash
window._ = require('lodash');

// Popper
window.Popper = require('popper.js').default;


// Bootstrap
require('bootstrap');

// Devbridge Jquery ajax autocomplete
require('devbridge-autocomplete');

// date time picker
global.moment = require('moment');
require('tempusdominus-bootstrap-4');


// bootbox
global.bootbox = require('bootbox');


// Axios
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = $('meta[name="csrf-token"]').attr('content');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

// Vue
// window.Vue = require('vue');
