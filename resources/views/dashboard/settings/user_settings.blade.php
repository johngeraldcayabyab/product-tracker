@extends('dashboard.layouts.dashboard_con')


@section('breadcrumbs')
    <!--Dashboard breadcrumbs-->
    <li class="breadcrumb-item active" aria-current="page">User settings</li>
    <!--End of dashboard breadcrumbs-->
@endsection


@section('actions')
    <!--Create and search-->
    <div class="container-fluid p-2 shadow-sm" style="background-color: #FFFFFF;">
        <div class="row no-gutters">
            <div class="col-md-6 offset-md-6 text-right">
                <button class="btn btn-outline-secondary shadow-sm" type="button" data-toggle="modal" data-target="#create-currency">
                    Create
                </button>
            </div>
        </div>
    </div>
    <!--End of create and search-->
@endsection

@section('dashboard_content')

    <!--Error messages-->
    @include('layouts.error_message')
    <!--End of error messages-->


    <div class="row mb-3">
        <div class="col-md-10 offset-md-1">
            <div class="card rounded-0 shadow">
                <table class="table" style="margin-bottom: 0;">
                    <thead>
                        <tr>
                            <th scope="col">Username</th>
                            <th scope="col">Name</th>
                            <th scope="col" class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    @if(!$oUsers->isEmpty())
                        @foreach($oUsers as $oUser)
                            <tr class="currency-data-con">
                                <td class="username">{{$oUser->username}}</td>
                                <td class="country_code">{{$oUser->name}}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-primary btn-sm currency-edit-btn" data-currency-id="{{$oUser->id}}" data-toggle="modal" data-target="#update-currency">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-sm currency-delete-btn" data-currency-id="{{$oUser->id}}" data-toggle="modal" data-target="#delete-currency">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">
                                <div class="card text-center p-3 border-0">
                                    No results found
                                </div>
                            </td>
                        </tr>
                    @endif


                    </tbody>
                </table>
            </div>

            <div class="text-center mt-3">
                <div style="display: inline-block;">
                    {{ $oUsers->links() }}
                </div>
            </div>
        </div>
    </div>



    <!-- Create currency modal -->
    <div class="modal fade" id="create-currency" tabindex="-1" role="dialog" aria-labelledby="create-currency" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('processCreateUser')}}">
                    @csrf
                    <div class="modal-body">

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="username">Username</label>
                                <input id="username" value="" class="form-control" placeholder="Username" name="username" type="text">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="user-name">Name</label>
                                <input id="user-name" value="" class="form-control" placeholder="Name" name="name" type="text">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="Password">Password</label>
                                <input id="password" value="" class="form-control" placeholder="Password" name="password" type="password">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="confirm-password">Confirm password</label>
                                <input id="confirm-password" value="" class="form-control" placeholder="Confirm password" name="confirm_password" type="password">
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Update currency modal -->
    <div class="modal fade" id="update-currency" tabindex="-1" role="dialog" aria-labelledby="update-currency" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update <strong id="currency-title" class="text-info"></strong> user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('processEditUser')}}">
                    @csrf
                    <input type="hidden" name="id" id="update-currency-id" value="">
                    <div class="modal-body">

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="username">Username</label>
                                <input id="update-name" value="" class="form-control" placeholder="Name" name="username" type="text">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="update-user-name">Name</label>
                                <input id="update-user-name" value="" class="form-control" placeholder="Name" name="name" type="text">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <!--Delete currency modal-->
    <div class="modal fade" id="delete-currency" tabindex="-1" role="dialog" aria-labelledby="delete-currency" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="process-currency">Are you sure you want to delete <strong id="currency-delete-title" class="text-danger"></strong> user?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('processDeleteUser')}}">
                    @csrf
                    <div class="modal-body text-center">

                        <input type="hidden" id="delete-currency-id" name="id" value="">

                        <button type="submit" class="btn btn-danger">
                            Yes
                        </button>
                        <button type="btn" class="btn btn-info" data-dismiss="modal">
                            No
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>




@endsection


@section('customJs')
    <script>
        $( document ).ready(function(){

            $('.currency-edit-btn').click(function () {
                let currencyData = $(this).parents('.currency-data-con');
                let currencyId = $(this).data('currency-id');
                let currencyName = currencyData.find('.username').html();
                let currencyCode = currencyData.find('.country_code').html();
                let currencySymbol = currencyData.find('.currency_symbol').html();
                $('#currency-title').html(currencyName);
                $('#update-currency-id').val(currencyId);
                $('#update-name').val(currencyName);
                $('#update-user-name').val(currencyCode);
                $('#update-symbol').val(currencySymbol);
            });

            $('.currency-delete-btn').click(function () {
                let currencyName = $(this).parents('.currency-data-con').find('.username').html();
                let currencyId = $(this).data('currency-id');
                $('#delete-currency-id').val(currencyId);
                $('#currency-delete-title').html(currencyName);
            });

        });
    </script>
@endsection