@extends('dashboard.layouts.dashboard_con')

@section('breadcrumbs')
    <!--Dashboard breadcrumbs-->
    <li class="breadcrumb-item active" aria-current="page">Product Categories</li>
    <!--End of dashboard breadcrumbs-->
@endsection



@section('actions')
    <!--Create and search-->
    <div class="container-fluid p-2 shadow-sm" style="background-color: #FFFFFF;">
        <div class="row no-gutters">
            <div class="col-md-6 offset-md-6 text-right">
                <button class="btn btn-outline-secondary shadow-sm" type="button" data-toggle="modal" data-target="#create-category">
                    Create
                </button>
            </div>
        </div>
    </div>
    <!--End of create and search-->

@endsection


@section('dashboard_content')



    <!--Error messages-->
    @include('layouts.error_message')
    <!--End of error messages-->


    <div class="row mb-3">
        <div class="col-md-10 offset-md-1">
            <div class="card rounded-0 shadow">
                <table class="table" style="margin-bottom: 0;" id="product-category-table">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Parent category</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if($aProductCategories)
                        @foreach($aProductCategories as $aProductCategory)
                            <tr class="inventory-category-data-con">
                                <td class="inventory-category-name">{{title_case($aProductCategory['name'])}}</td>
                                <td class="inventory-category-name-parent">
                                    {{$aProductCategory['theParent'] ? title_case($aProductCategory['theParent']) : 'N/A'}}
                                </td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-primary inventory-category-edit-btn" data-inventory-category-id="{{$aProductCategory['id']}}" data-toggle="modal" data-target="#update-product-category">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-danger inventory-category-delete-btn" data-inventory-category-id="{{$aProductCategory['id']}}" data-toggle="modal" data-target="#delete-inventory-category">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">
                                <div class="card text-center p-3 border-0">
                                    No results found
                                </div>
                            </td>
                        </tr>
                    @endif



                    </tbody>
                </table>
            </div>
        </div>
    </div>




    <!-- Create inventory category modal -->
    <div class="modal fade" id="create-category" tabindex="-1" role="dialog" aria-labelledby="create-category" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('processCreateProductCategory')}}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Name</label>
                                <input id="name" value="" class="form-control" placeholder="Name" name="name" type="text">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="parent-category">Parent category</label>
                                <select name="parent_category_id" class="form-control" id="parent-category">
                                    <option disabled selected value>Choose your option</option>
                                    @foreach($aProductCategories as $aProductCategory)
                                        <option value="{{$aProductCategory['id']}}">{{$aProductCategory['theParent'] !== false ? title_case($aProductCategory['theParent'] . ' / ' . $aProductCategory['name']) : title_case($aProductCategory['name'])}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Update inventory category modal -->
    <div class="modal fade" id="update-product-category" tabindex="-1" role="dialog" aria-labelledby="update-product-category" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Update <strong id="inventory-category-title" class="text-info"></strong> category</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('processUpdateProductCategory')}}">
                    @csrf
                    <input type="hidden" name="id" id="update-inventory-category-id" value="">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="update-name">Name</label>
                                <input id="update-name" value="" class="form-control" placeholder="Name" name="name" type="text">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="update-inventory-category-name-parent">Parent category</label>
                                <select name="parent_category_id" class="form-control" id="update-inventory-category-name-parent">
                                    <option disabled selected value>Choose your option</option>
                                    @foreach($aProductCategories as $aProductCategory)
                                        <option value="{{$aProductCategory['id']}}">{{$aProductCategory['theParent'] !== false ? title_case($aProductCategory['theParent'] . ' / ' . $aProductCategory['name']) : title_case($aProductCategory['name'])}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>





    <!--Delete inventory category modal-->
    <div class="modal fade" id="delete-inventory-category" tabindex="-1" role="dialog" aria-labelledby="delete-product-category" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Are you sure you want to delete <strong id="inventory-category-delete-title" class="text-danger"></strong> product category?</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{route('processDeleteProductCategory')}}">
                    @csrf
                    <div class="modal-body text-center">

                        <input type="hidden" id="delete-inventory-category-id" name="id" value="">

                        <button type="submit" class="btn btn-danger">
                            Yes
                        </button>
                        <button type="btn" class="btn btn-info" data-dismiss="modal">
                            No
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection


@section('customJs')
    <script>
        $(document).ready(function() {

            $('.inventory-category-edit-btn').click(function () {
                // alert($(this).data('inventory-category-id'));
                let productCategoryData = $(this).parents('.inventory-category-data-con');
                let productOd = $(this).data('inventory-category-id');
                let productCategoryName = productCategoryData.find('.inventory-category-name').html();
                let productCategoryNameParent = productCategoryData.find('.inventory-category-name-parent').html().trim();
                $('#inventory-category-title').html(productCategoryName);
                $('#update-inventory-category-id').val(productOd);
                $('#update-name').val(productCategoryName);
                $('#update-inventory-category-name-parent option:first').attr("selected","selected");
                $('#update-inventory-category-name-parent option').each(function(){
                    if ($(this).text() === productCategoryNameParent){
                        console.log(productCategoryNameParent);
                        $(this).attr("selected","selected");
                    }
                });

            });

            // Remove all selected attributes in the option when modal is closed in update inventory categories
            $('#update-inventory-category').on('hidden.bs.modal', function () {
                $('#update-inventory-category-name-parent option').each(function(){
                    $(this).removeAttr('selected');
                });
            });


            // Product category delete
            $('.inventory-category-delete-btn').click(function () {
                let sProductCategoryName = $(this).parents('.inventory-category-data-con').find('.inventory-category-name').html();
                let iProductId = $(this).data('inventory-category-id');
                $('#delete-inventory-category-id').val(iProductId);
                $('#inventory-category-delete-title').html(sProductCategoryName);
            });

        } );
    </script>
@endsection