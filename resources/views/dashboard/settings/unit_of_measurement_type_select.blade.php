@extends('dashboard.layouts.dashboard_con')


@section('breadcrumbs')
    <!--Dashboard breadcrumbs-->
    <li class="breadcrumb-item active" aria-current="page">Unit of measurement type</li>
    <li class="breadcrumb-item active" aria-current="page">Select</li>
    <!--End of dashboard breadcrumbs-->
@endsection


@section('dashboard_content')
    <!--Error messages-->
    @include('layouts.error_message')
    <!--End of error messages-->

    <div class="row">
        <div class="col-md-8 offset-md-2">
            <form method="post" action="{{route('processSelectUnitOfMeasurementType')}}">
                <div class="card rounded-0 shadow">

                    @csrf
                    <div class="card-body text-center">
                        <div class="card-title pt-3"><h6 class="text-info text-center">Please select the unit of measurement type you prefer.</h6></div>

                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="unit_of_measurement_type" id="metric" value="metric">
                            <label class="form-check-label" for="metric">Metric system</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="unit_of_measurement_type" id="imperial" value="imperial">
                            <label class="form-check-label" for="imperial">Imperial system</label>
                        </div>

                        <div class="text-center mt-3">

                            <button type="submit" class="btn btn-success" style="background-color: #00d994; border-color: #00d994;">Submit</button>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <small class="text-info">Choose wisely because this can't be changed anymore.</small>
                    </div>

                </div>



            </form>


        </div>


    </div>



@endsection








