@extends('dashboard.layouts.dashboard_con')

@section('breadcrumbs')
    <!--Dashboard breadcrumbs-->
    <li class="breadcrumb-item active" aria-current="page">Units of measurement</li>
    <!--End of dashboard breadcrumbs-->
@endsection


@section('actions')
    <!--Create and search-->
        <div class="container-fluid p-2 shadow-sm" style="background-color: #FFFFFF;">
            <div class="row no-gutters">
                <div class="col-md-6 offset-md-6 text-right">
                    <button class="btn btn-outline-secondary shadow-sm" type="button">
                        <span class="text-info">{{ucfirst($oUnitOfMeasurementType['data']->type)}}</span> is currently active.
                    </button>
                </div>
            </div>
        </div>
    <!--End of create and search-->

@endsection

@section('dashboard_content')


    <!--Error messages-->
    @include('layouts.error_message')
    <!--End of error messages-->

    <div class="row">
        <div class="col-3">
            <div class="nav flex-column nav-pills bg-white text-center shadow" role="tablist" aria-orientation="vertical">
                <a class="nav-link {{$oUnitOfMeasurementType['data']->type === 'metric' ? 'active' : ''}} rounded-0" id="tab-link-1" data-toggle="pill" href="#tab-content-1" role="tab" aria-controls="tab-content-1" aria-selected="true">Metric</a>
                <a class="nav-link {{$oUnitOfMeasurementType['data']->type === 'imperial' ? 'active' : ''}} rounded-0" id="tab-link-2" data-toggle="pill" href="#tab-content-2" role="tab" aria-controls="tab-content-2" aria-selected="true">Imperial</a>
            </div>
        </div>
        <div class="col-9">
            <div class="tab-content">
                <div class="tab-pane fade {{$oUnitOfMeasurementType['data']->type === 'metric' ? 'show active' : ''}}" id="tab-content-1" role="tabpanel" aria-labelledby="tab-link-1">
                    <div class="card rounded-0 shadow">
                        <table class="table" style="margin-bottom: 0;">
                            <thead>
                            <tr>
                                <th scope="col" style="border-top: none;">Name</th>
                                <th scope="col" style="border-top: none;">Abbreviation</th>
                                <th scope="col" style="border-top: none;">Measurement type</th>
                                <th scope="col" style="border-top: none;">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($oUnitsOfMeasurement as $oMetricSystem)
                                    @if($oMetricSystem->unit_of_measurement_type === 'metric' || $oMetricSystem->unit_of_measurement_type === 'pieces')
                                        <tr class="data-con">
                                            <td>{{$oMetricSystem->name}}</td>
                                            <td>{!!$oMetricSystem->abbr!!}</td>
                                            <td>{{$oMetricSystem->measurement_type}}</td>
                                            <td>
                                                @if($oMetricSystem->active === 1)
                                                    <span class="badge badge-success">Active</span>
                                                @else
                                                    <span class="badge badge-secondary">Inactive</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="tab-pane fade {{$oUnitOfMeasurementType['data']->type === 'imperial' ? 'show active' : ''}}" id="tab-content-2" role="tabpanel" aria-labelledby="tab-link-2">
                    <div class="card">
                        <table class="table" style="margin-bottom: 0;">
                            <thead>
                            <tr>
                                <th scope="col" style="border-top: none;">Name</th>
                                <th scope="col" style="border-top: none;">Abbreviation</th>
                                <th scope="col" style="border-top: none;">Measurement type</th>
                                <th scope="col" style="border-top: none;">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($oUnitsOfMeasurement as $oImperialSystem)
                                @if($oImperialSystem->unit_of_measurement_type === 'imperial' || $oImperialSystem->unit_of_measurement_type === 'pieces')
                                    <tr class="data-con">
                                        <td>{{$oImperialSystem->name}}</td>
                                        <td>{!!$oImperialSystem->abbr!!}</td>
                                        <td>{{$oImperialSystem->measurement_type}}</td>
                                        <td>
                                            @if($oImperialSystem->active === 1)
                                                <span class="badge badge-success">Active</span>
                                            @else
                                                <span class="badge badge-secondary">Inactive</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="row mb-3">--}}
        {{--<div class="col-md-10 offset-md-1">--}}
            {{--<table class="table table-striped" id="unit_of_measurement-table">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                    {{--<th scope="col">Name</th>--}}
                    {{--<th scope="col">Actions</th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}

                {{--@foreach($oUnitsOfMeasurement as $oUnitOfMeasurement)--}}
                    {{--<tr class="unit-of-measurement-data-con">--}}
                        {{--<td class="unit_of_measurement_name">{{title_case($oUnitOfMeasurement['name'])}}</td>--}}
                        {{--<td>--}}
                            {{--<button type="button" class="btn btn-primary unit-of-measurement-edit-btn" data-unit-of-measurement-id="{{$oUnitOfMeasurement['id']}}" data-toggle="modal" data-target="#update-unit-of-measurement">--}}
                                {{--<i class="fas fa-edit"></i>--}}
                            {{--</button>--}}
                            {{--<button type="button" class="btn btn-danger unit-of-measurement-delete-btn" data-unit-of-measurement-id="{{$oUnitOfMeasurement['id']}}" data-toggle="modal" data-target="#delete-unit-of-measurement">--}}
                                {{--<i class="fas fa-trash"></i>--}}
                            {{--</button>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}

                {{--</tbody>--}}
            {{--</table>--}}
        {{--</div>--}}
    {{--</div>--}}


    {{--<!-- Create units-of-measurement modal -->--}}
    {{--<div class="modal fade" id="create-units-of-measurement" tabindex="-1" role="dialog" aria-labelledby="create-units-of-measurement" aria-hidden="true">--}}
        {{--<div class="modal-dialog modal-dialog-centered" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<h5 class="modal-title">Create unit of measurement</h5>--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                    {{--</button>--}}
                {{--</div>--}}
                {{--<form method="post" action="{{route('processCreateUnitsOfMeasurement')}}">--}}
                    {{--@csrf--}}
                    {{--<div class="modal-body">--}}

                        {{--<div class="form-row">--}}
                            {{--<div class="form-group col-md-12">--}}
                                {{--<label for="name">Name</label>--}}
                                {{--<input id="name" value="" class="form-control" placeholder="Name" name="name" type="text">--}}
                            {{--</div>--}}

                        {{--</div>--}}

                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                        {{--<button type="submit" class="btn btn-primary">Create</button>--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}



    {{--<!-- Update unit_of_measurement modal -->--}}
    {{--<div class="modal fade" id="update-unit-of-measurement" tabindex="-1" role="dialog" aria-labelledby="update-unit-of-measurement" aria-hidden="true">--}}
        {{--<div class="modal-dialog modal-dialog-centered" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<h5 class="modal-title">Update <strong id="unit-of-measurement-title" class="text-info"></strong> Unit of measurement</h5>--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                    {{--</button>--}}
                {{--</div>--}}
                {{--<form method="post" action="{{route('processUpdateUnitOfMeasurement')}}">--}}
                    {{--@csrf--}}
                    {{--<input type="hidden" name="id" id="update-unit-of-measurement-id" value="">--}}
                    {{--<div class="modal-body">--}}

                        {{--<div class="form-row">--}}
                            {{--<div class="form-group col-md-6">--}}
                                {{--<label for="update-name">Name</label>--}}
                                {{--<input id="update-name" value="" class="form-control" placeholder="Name" name="name" type="text">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
                        {{--<button type="submit" class="btn btn-primary">Update</button>--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


    {{--<!--Delete unit-of-measurement modal-->--}}
    {{--<div class="modal fade" id="delete-unit-of-measurement" tabindex="-1" role="dialog" aria-labelledby="delete-unit-of-measurement" aria-hidden="true">--}}
        {{--<div class="modal-dialog modal-dialog-centered" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<h5 class="modal-title" id="process-unit-of-measurement">Are you sure you want to delete <strong id="unit-of-measurement-delete-title" class="text-danger"></strong> unit of measurement?</h5>--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                    {{--</button>--}}
                {{--</div>--}}
                {{--<form method="post" action="{{route('processDeleteUnitOfMeasurement')}}">--}}
                    {{--@csrf--}}
                    {{--<div class="modal-body text-center">--}}

                        {{--<input type="hidden" id="delete-unit-of-measurement-id" name="id" value="">--}}

                        {{--<button type="submit" class="btn btn-danger">--}}
                            {{--Yes--}}
                        {{--</button>--}}
                        {{--<button type="btn" class="btn btn-info" data-dismiss="modal">--}}
                            {{--No--}}
                        {{--</button>--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}




@endsection


@section('customJs')
    {{--<script>--}}
        {{--$( document ).ready(function(){--}}

            {{--$('.unit-of-measurement-edit-btn').click(function () {--}}
                {{--let unitOfMeasurementData = $(this).parents('.unit-of-measurement-data-con');--}}
                {{--let unitOfMeasurementId = $(this).data('unit-of-measurement-id');--}}
                {{--let unitfOfMeasurementName = unitOfMeasurementData.find('.unit_of_measurement_name').html();--}}
                {{--$('#unit-of-measurement-title').html(unitfOfMeasurementName);--}}
                {{--$('#update-unit-of-measurement-id').val(unitOfMeasurementId);--}}
                {{--$('#update-name').val(unitfOfMeasurementName);--}}
            {{--});--}}

            {{--$('.unit-of-measurement-delete-btn').click(function () {--}}
                {{--let unitOfMeasurementName = $(this).parents('.unit-of-measurement-data-con').find('.unit_of_measurement_name').html();--}}
                {{--let unitOfMeasurementId = $(this).data('unit-of-measurement-id');--}}
                {{--$('#delete-unit-of-measurement-id').val(unitOfMeasurementId);--}}
                {{--$('#unit-of-measurement-delete-title').html(unitOfMeasurementName);--}}
            {{--});--}}

        {{--});--}}
    {{--</script>--}}
@endsection