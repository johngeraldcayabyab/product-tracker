

<nav class="navbar navbar-expand-lg dashboard-top-nav shadow-sm" id="dashboard-top-nav-con" style="height: 56px; background-color: #FFFFFF; z-index: 9;">

    <!--Company logo-->
    {{--<a class="navbar-brand company-logo-con" href="#">--}}
        {{--<img src="{{asset('img/logos/company_logo.png')}}" class="responsive-img company_logo">--}}
    {{--</a>--}}
    <!--End of company logo-->

    <!--Burger nav-->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#head-nav-collapse" aria-controls="head-nav-collapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!--End of burger nav-->

    <!--Everything hear will be collapsed when the browser is minimized-->
    <div class="collapse navbar-collapse" id="head-nav-collapse">

        <ul class="navbar-nav">
            <!--Breadcrumbs for now?-->
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb" style="margin-bottom: 0; background: none;">
                        @yield('breadcrumbs')
                    </ol>
                </nav>
            <!--End of breadcrumbs for now-->
        </ul>

        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            {{--<li class="nav-item {{url()->current() === route('showHome') ? 'active' : 0}}">--}}
                {{--<a class="nav-link" href="{{route('showHome')}}">HOME</a>--}}
            {{--</li>--}}

            {{--<li class="nav-item {{url()->current() === route('showCompanyProfile') ? 'active' : 0}}">--}}
                {{--<a class="nav-link" href="{{route('showCompanyProfile')}}">ADMIN</a>--}}
            {{--</li>--}}

            {{--<li class="nav-item {{url()->current() === route('showProducts') ? 'active' : 0}}">--}}
                {{--<a class="nav-link" href="{{route('showProducts')}}">PRODUCTS</a>--}}
            {{--</li>--}}

            {{--<li class="nav-item {{url()->current() === route('showPosProducts') ? 'active' : 0}}">--}}
                {{--<a class="nav-link" href="{{route('showPosProducts')}}">POINT OF SALE</a>--}}
            {{--</li>--}}

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="settings-link-dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    SETTINGS
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelled-by="settings-link-dropdown">
                    <a class="dropdown-item" href="{{route('showUserProfile')}}">PROFILE</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logOut') }}">LOG OUT</a>
                </div>
            </li>
        </ul>
    </div>
    <!--end of verything hear will be collapsed when the browser is minimized-->
</nav>



@yield('actions')





