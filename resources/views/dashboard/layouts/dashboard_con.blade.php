@extends('layouts.master')

@section('customCss')
    <link rel="stylesheet" type="text/css" href="{{asset('css/dashboard/dashboard.css')}}">
@endsection

@section('content')

    <!--Dashboard container-->
    {{--<div id="dashboard-con" class="row no-gutters">--}}

    <div id="dashboard-con" class="row no-gutters" style="height: 100%;">

        <!--Left side navigation-->
        <div id="dashboard-side-nav-con" style="width: 20%; background-color: #232f49; position: relative; z-index: 10; overflow-y: auto;">
            @include('dashboard.layouts.side_nav')
        </div>
        <!--End of left side navigation-->

        <!--Dashboard content-->
        <div id="dashboard-main-section-con" style="width: 80%; overflow-y: auto;">

            <!--Top navigation-->
                @include('dashboard.layouts.head_nav')
            <!--End of top navigation-->

            <div class="p-3" style="height: 100%; position: relative;">
                @yield('dashboard_content')
            </div>
        </div>
        <!--End of dashboard content-->

    </div>
    <!--End of dashboard container-->

@endsection

@section('containerJs')
    <script type="text/javascript" src="{{asset('js/dashboard/dashboard.js')}}"></script>
@endsection
