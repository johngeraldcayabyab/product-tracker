<ul class="nav flex-column accordion">

    {{--  --}}
    <li style="height: 56px; padding: .5rem 1rem; background: #00d994;">
        <a class="navbar-brand company-logo-con text-center" href="#">
            <img src="{{$companyLogo}}" class="" style="height: 40px;">
        </a>
    </li>

    @foreach($aSideNavLinks as $mLink)
        @if(strpos(url()->current(), $mLink['base_url']) === false)
            <li id="{{$mLink['id']}}-links" class="nav-item side-nav-links" data-toggle="collapse" data-target="#{{$mLink['id']}}-sub-links" aria-expanded="true" aria-controls="{{$mLink['id']}}-sub-links">
                <a class="nav-link side-nav-links-href" href="#"><i class="fas fa-{{$mLink['icon']}} pr-1"></i> {{$mLink['link']}}</a>
                <i class="fas fa-chevron-up side-nav-links-icon-status rotate"></i>
            </li>

            <li id="{{$mLink['id']}}-sub-links" class="nav-item collapse side-nav-sub-links" aria-labelledby="{{$mLink['id']}}-sub-links" data-parent="#accordion">
                @foreach($mLink['sub_links'] as $sLink => $sSubUrl)
                    <a class="nav-link side-nav-sub-links-href pt-3 pb-3" href="{{$sSubUrl}}" data-parent-link="{{$mLink['id']}}-links"><span class="pr-2 sub-nav-sub-links-bullets">&#x26AC;</span>{{$sLink}}</a>
                @endforeach
            </li>
        @else
            <li id="{{$mLink['id']}}-links" class="nav-item side-nav-links side-nav-links-active" data-toggle="collapse" data-target="#{{$mLink['id']}}-sub-links" aria-expanded="true" aria-controls="{{$mLink['id']}}-sub-links">
                <a class="nav-link side-nav-links-href" href="#"><i class="fas fa-{{$mLink['icon']}} pr-1"></i> {{$mLink['link']}}</a>
                <i class="fas fa-chevron-up rotate down side-nav-links-icon-status"></i>
            </li>

            <li id="{{$mLink['id']}}-sub-links" class="nav-item collapse side-nav-sub-links show" aria-labelledby="{{$mLink['id']}}-sub-links" data-parent="#accordion">
                @foreach($mLink['sub_links'] as $sLink => $sSubUrl)
                    <a class="nav-link side-nav-sub-links-href pt-3 pb-3 {{strpos(url()->current(), $sSubUrl) === false ? '' : 'side-nav-sub-links-active'}}" href="{{$sSubUrl}}" data-parent-link="{{$mLink['id']}}-links"><span class="pr-2 sub-nav-sub-links-bullets">&#x26AC;</span>{{$sLink}}</a>
                @endforeach
            </li>
        @endif
    @endforeach


</ul>

