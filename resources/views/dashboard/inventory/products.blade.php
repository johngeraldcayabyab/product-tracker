@extends('dashboard.layouts.dashboard_con')


@section('breadcrumbs')
    <!--Dashboard breadcrumbs-->
    <li class="breadcrumb-item active" aria-current="page">Products</li>
    <!--End of dashboard breadcrumbs-->
@endsection

@section('actions')
    <!--Create and search-->
    <form id="advance-product-search">
    <div class="container-fluid p-2 shadow-sm" style="background-color: #FFFFFF;">
        <div class="row no-gutters">
            <div class="col-md-6 border-right">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary  border-0" type="button"><i class="fas fa-search"></i></button>
                    </div>
                    <input type="text" class="form-control  border-0" name="name" placeholder="Search products">
                    <div class="input-group-append">
                        <button type="button" class="btn btn-outline-secondary border-0" data-toggle="collapse" data-target="#advance-search" aria-expanded="true" aria-controls="advance-search">Advance search <i class="fas fa-caret-down"></i></button>
                    </div>
                </div>
            </div>

            <div class="col-md-6 text-right">
                <a class="btn btn-outline-secondary shadow-sm" href="{{route('showCreateProducts', ['iId' => 0])}}">Create</a>
            </div>
        </div>


    </div>

        <div id="advance-search" class="collapse shadow-sm" style="background-color: #ffffff;">
            <div class="card-body">

                <div class="form-row">

                    <div class="form-group col-md-4">
                    <label for="limit">Entries</label>
                    <select name="limit" class="form-control" id="limit">
                    <option value="5" selected>Show 5 entries</option>
                    <option value="10">Show 10 entries</option>
                    <option value="20">Show 20 entries</option>
                    </select>
                    </div>

                    <div class="form-group col-md-4">
                    <label for="product_type">Product type</label>
                    <select name="product_type" class="form-control" id="product_type">
                    <option value="" selected>All</option>
                    <option value="Stockable">Stockable</option>
                    <option value="Consumable">Consumable</option>
                    <option value="Service">Service</option>
                    </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="product_category">Product category</label>
                        <select name="product_category" class="form-control" id="product_category">
                            <option value="" selected>All</option>
                            @foreach($oProductCategories as $oProductCategory)
                                <option value="{{$oProductCategory['id']}}">{{$oProductCategory['name']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="measurement_type">Measurement type</label>
                        <select name="measurement_type" id="measurement_type" class="form-control">
                            <option value="" selected>All</option>
                            @foreach($oUnitsOfMeasurements as $oUnitsOfMeasurement)
                                <option value="{{$oUnitsOfMeasurement->id}}">{{$oUnitsOfMeasurement->name}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>


            </div>
        </div>
    </form>
    <!--End of create and search-->

@endsection


@section('dashboard_content')





    <!--Error messages-->
    @include('layouts.error_message')
    <!--End of error messages-->


    <!--Display data (kanban type)-->
    <div class="row no-gutters" id="products-con">
        {{--@if($oDatas->isEmpty() === false)--}}
            {{--@foreach($oDatas as $oData)--}}
                {{--<div class="col-lg-3 col-md-4 col-sm-6 col-xs-1">--}}
                    {{--<a class="kanban-link-con mb-3" href="{{route('showEditProducts', ['iId' => $oData['id']])}}">--}}
                        {{--<img src="{{asset('img/raw_materials/default.png')}}" class="kanban-link-img">--}}
                        {{--<p class="kanban-link-content">--}}
                            {{--<strong>{{$oData->name}}</strong>--}}
                            {{--<span>Quantity: {{$oData->quantity}} {{$oData['measurementType']['name']}}</span>--}}
                            {{--<span>Sales price: {{$oData->sales_price}}</span>--}}
                        {{--</p>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--@endforeach--}}
        {{--@else--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-body">--}}
                        {{--<h6 class="text-center">No results found</h6>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endif--}}
    </div>
    <!--End of display data (kanban type)-->


    <!--Products pagination-->
    <nav id="products-pagination-con" class="row">
    </nav>
    <!--End of products pagination-->


@endsection

@section('customJs')
    <script>
        $( document ).ready(function(){
            getProducts(generateProductsUrl());
            $("#advance-product-search :input").on('change keyup', function(){
                getProducts(generateProductsUrl());
            });
            // Ajax call when pagination product link click , yeah my sentence sucks
            $('#products-pagination-con').on('click', '.products-page-link', function(){
                event.preventDefault();
                getProducts($(this).attr('href'));
            });
        });

        function getProducts(sProductsUrl = null)
        {
            showNeutron();
            let sDefaultUrl = '{{route('getProducts')}}';
            if(sProductsUrl){
                sDefaultUrl  = sProductsUrl;
            }
            $.get(sDefaultUrl, function(oData){
                hideNeutron();
                generateProducts(oData.data.data);
                generatePaginationLinks(oData.data.last_page, oData.data.current_page, sProductsUrl);
            });
        }

        function generateProducts(oProducts)
        {
            $('#products-con').html('');
            $.each(oProducts, function(key, value){
                let sProductUrl = '{{route('showCreateProducts', ['iId' => ':iId'])}}';
                let sQuantityOnHand = value.quantity_on_hand;
                let sSalesPrice = value.sales_price;

                let sStorageUrl = '{{asset('storage/images/products')}}' + '/' + value.image_name;
                let sDefaultImgUrl = '{{asset('img/raw_materials/default.png')}}';

                let sImgSrc = value.image_name ? sStorageUrl :  sDefaultImgUrl;


                sProductUrl = sProductUrl.replace(':iId', value.id);
                if(sQuantityOnHand){
                    sQuantityOnHand = parseFloat(sQuantityOnHand).toString();
                }
                if(sQuantityOnHand === null || sQuantityOnHand === '0'){
                    sQuantityOnHand = ' <small class="text-danger">Out of stock</small>';
                }
                if(value.product_type === 'Consumable'){
                    sQuantityOnHand = ' <small class="text-info">Consumable</small>';
                }else if(value.product_type === 'Service'){
                    sQuantityOnHand = ' <small class="text-info">Service</small>';
                }
                if(sSalesPrice){
                    sSalesPrice = parseFloat(sSalesPrice).toString();
                }
                if(sSalesPrice === null){
                    sSalesPrice = 0;
                }
                $('#products-con').append(
                    '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-1">' +
                    '<a class="kanban-link-con mb-3" href="' + sProductUrl + '">' +
                    '<img src="' + sImgSrc + '" class="kanban-link-img">' +
                    '<p class="kanban-link-content">' +
                    '<strong>' + value.name + '</strong>'+
                    '<span>Quantity: ' + sQuantityOnHand + '</span>' +
                    '<span>Sales price: ' + '{{$oCurrency->symbol or ''}}' + sSalesPrice + '</span>' +
                    '</p>' +
                    '<div class="clearfix"></div></a></div>');
            });
        }


        function generateProductsUrl()
        {
            return '{{route('getProducts')}}' + '?' + $('#advance-product-search').serialize();
        }

        function generatePaginationLinks(iLastPage, iCurrentPage, sProductUrl)
        {
            let productsPaginationCon = $('#products-pagination-con');
            let paginationLinksHtml = '<div class="col-md-6 offset-md-3 py-1 d-flex"><ul class="pagination mx-auto">';
            for(let i = 1; i <= iLastPage; i++)
            {
                let sActive = i === iCurrentPage ? 'active' : '';
                paginationLinksHtml += '<li class="page-item ' + sActive + '"><a class="page-link products-page-link" href="' + sProductUrl +  '&page=' + i + '">'+ i +'</a></li>';
            }
            paginationLinksHtml += '</ul></div>';
            productsPaginationCon.html(paginationLinksHtml);
            if(iLastPage === 1){
                productsPaginationCon.html('');
            }
        }
    </script>
@endsection