@extends('dashboard.layouts.dashboard_con')


@section('breadcrumbs')
    <!--Dashboard breadcrumbs-->
    <li class="breadcrumb-item" aria-current="page"><a href="{{route('showProducts')}}">Products</a></li>
    <li class="breadcrumb-item active" aria-current="page">{!!isset($oProduct) ? 'Edit <strong>' . $oProduct->name . '</strong>' : 'Create'!!}</li>
    <!--End of dashboard breadcrumbs-->
@endsection

@section('dashboard_content')

    <!--Error messages-->
    @include('layouts.error_message')
    <!--End of error messages-->

    <!--Tabs-->
    <nav class="row">
        <div class="col-md-8 offset-md-2">
            <div class="nav nav-tabs" role="tablist">
                <a class="nav-item nav-link active" id="tab-link-1" data-toggle="tab" href="#tab-info-1" role="tab" aria-controls="tab-info-1" aria-selected="true">Product details</a>
                <a class="nav-item nav-link" id="tab-link-2" data-toggle="tab" href="#tab-info-2" role="tab" aria-controls="tab-info-2" aria-selected="false">Bill of Materials</a>
            </div>
        </div>
    </nav>
    <!--End of tabs-->

    <!--Create con-->
    @if(isset($oProduct))
        <form class="row tab-content" method="post" action="{{route('processEditProducts')}}" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{$oProduct->id}}">
    @else
        <form class="row tab-content" method="post" action="{{route('processCreateProducts')}}" enctype="multipart/form-data">
    @endif

        @csrf
        <div class="col-md-8 offset-md-2 tab-pane fade show active" id="tab-info-1" role="tabpanel" aria-labelledby="tab-link-1">
            <div class="card">
                <div class="card-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Name</label>
                            <input class="form-control" placeholder="Name" name="name" id="name" type="text" value="{{old('name', isset($oProduct->name) ? $oProduct->name : '')}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="product-img">Product image</label>
                            @if(isset($oProduct->image_name))
                                <span class="text-success">Image is set</span>
                            @elseif(isset($oProduct) && $oProduct->image_name === null)
                                <span class="text-danger">Image is not set</span>
                            @endif
                            <input type="file" class="form-control-file" id="product-img" name="image_name">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="sales_price">Sales price</label>
                            <input placeholder="Sales price" name="sales_price" id="sales_price" type="number" step="any" class="form-control" value="{{(float)old('sales_price', isset($oProduct->sales_price) ? (float)$oProduct->sales_price : '')}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="cost_price">Cost price</label>
                            <input placeholder="Cost price" name="cost_price" id="cost_price" type="number" step="any" class="form-control" value="{{(float)old('cost_price', isset($oProduct->cost_price) ? (float)$oProduct->cost_price : '')}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="product_type_input">Product type</label>
                            <select name="product_type" class="form-control" id="product_type_input">
                                <option value="" disabled {{isset($oProduct->product_type) ? '' : 'selected'}}>Choose your option</option>
                                @foreach(['Stockable', 'Consumable', 'Service'] as $sProductType)
                                    @if(old('product_type') === $sProductType)
                                        <option value="{{$sProductType}}" selected>{{$sProductType}}</option>
                                    @else
                                        <option value="{{$sProductType}}" {{isset($oProduct->product_type) && $oProduct->product_type === $sProductType ? 'selected' : ''}}>{{$sProductType}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="product_category">Product category</label>
                            <select name="product_category" class="form-control" id="product_category">
                                <option value="" disabled {{isset($oProduct->product_category) ? '' : 'selected'}}>Choose your option</option>
                                @foreach($oProductCategories as $oProductCategory)
                                    @if((int)old('product_category') === (int)$oProductCategory['id'])
                                        <option value="{{$oProductCategory['id']}}" selected>
                                            {{$oProductCategory['theParent'] ? $oProductCategory['theParent'] . ' /' : ''}} {{$oProductCategory['name']}}
                                        </option>
                                    @else
                                        <option value="{{$oProductCategory['id']}}" {{isset($oProduct->product_category) && (int)$oProduct->product_category === (int)$oProductCategory['id'] ? 'selected' : ''}}>
                                            {{$oProductCategory['theParent'] ? $oProductCategory['theParent'] . ' /' : ''}} {{$oProductCategory['name']}}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="sku">SKU</label>
                            <input placeholder="SKU" name="sku" id="sku" type="text" class="form-control" value="{{old('sku', isset($oProduct->sku) ? $oProduct->sku : '')}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="upc">UPC</label>
                            <input placeholder="UPC" name="upc" id="upc" type="text" class="form-control" value="{{old('upc', isset($oProduct->upc) ? $oProduct->upc : '')}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="measurement_type">Measurement type</label>
                            <select name="measurement_type" id="measurement_type" class="form-control">
                                <option value="" disabled selected>Choose your option</option>
                                @foreach($oMeasurements as $oMeasurement)
                                    @if((int)old('measurement_type') === (int)$oMeasurement->id)
                                        <option value="{{$oMeasurement->id}}" selected>{{$oMeasurement->name}} ({{$oMeasurement->measurement_type}})</option>
                                    @else
                                        <option value="{{$oMeasurement->id}}" {{isset($oProduct->measurement_type) && (int)$oProduct->measurement_type === (int)$oMeasurement->id ? 'selected' : ''}}>{{$oMeasurement->name}} ({{$oMeasurement->measurement_type}})</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6" id="quantity-on-hand-con"  {!!isset($oProduct->product_type) && $oProduct->product_type === 'Stockable' ? '' : 'style="display: none;"'!!}>
                            <label for="quantity-on-hand-input">Quantity on hand</label>
                            <input class="form-control" placeholder="Quantity on hand" id="quantity-on-hand-input" type="number" step="any" name="quantity_on_hand" value="{{old('quantity_on_hand', isset($oProduct->quantity_on_hand) ? $oProduct->quantity_on_hand : '')}}">
                        </div>

                        <div class="form-group col-md-12 text-center">
                            <button type="submit" class="btn btn-success" style="background-color: #00d994; border-color: #00d994;">{{isset($oProduct) ? 'Edit' : 'Create'}}</button>
                            @if(isset($oProduct))
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-product-modal">Delete</button>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-8 offset-md-2 tab-pane fade" role="tabpanel" id="tab-info-2" aria-labelledby="tab-link-2">
            <div class="card">
                <div class="card-body">
                    <div id="bom-body">
                        @if(isset($oBoms))
                            @foreach($oBoms as $oBom)
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <input type="hidden" name="bom_id[]" value="{{$oBom->products->id}}">
                                        <input type="text" class="form-control bom-name" placeholder="Name" value="{{$oBom->products->name}}" readonly>
                                    </div>
                                    <div class="form-group col-md-5">
                                        <input type="number" name="bom_quantity[]" class="form-control" value="{{(float)$oBom->quantity}}" placeholder="Quantity">
                                    </div>
                                    <div class="form-group col-md-2 text-center">
                                        <button class="btn btn-danger bom-delete" type="button">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12 text-center">
                            <button type="button" class="btn btn-outline-primary" id="add-bom-btn" data-toggle="modal" data-target="#add-bom-modal">Add BOM</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--End of create con-->



    <!-- Modal -->
    <div class="modal fade search-and-add-modal" id="add-bom-modal" tabindex="-1" role="dialog" aria-labelledby="add-bom-modal-label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="input-group">
                       <div class="input-group-prepend">
                           <button class="btn btn-outline-secondary border-0" type="button"><i class="fas fa-search"></i></button>
                           </div>
                        <input type="text" class="form-control border-0" id="search-product-names-input" placeholder="Search BoM">
                      </div>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                        <tr>
                           <th scope="col" class="text-center">Name</th>
                          <th scope="col" class="text-center">Quantity on hand</th>
                        <th scope="col" class="text-center">Actions</th>
                         </tr>
                        </thead>
                       <tbody id="search-add-modal-table-body">
                     </tbody>
                      </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary bg-white text-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


                <!-- Modal -->
                <div class="modal fade" id="delete-product-modal" tabindex="-1" role="dialog" aria-labelledby="delete-product-modal-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Are you sure you want to delete <span class="text-danger">{{$oProduct->name or ''}}</span>?</h5>
                            </div>
                            <form class="modal-body text-center" method="post" action="{{route('processDeleteProducts')}}">
                                @csrf
                                <input type="hidden" name="id" value="{{$oProduct->id or ''}}">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-success" style="background-color: #00d994; border-color: #00d994;" data-dismiss="modal">No</button>
                            </form>

                        </div>
                    </div>
                </div>



@endsection


@section('customJs')

    <script>

        $( document ).ready(function(){

            const productNamesAndQuantityUrl = '{{route('getProductNamesAndQuantity')}}';

            $('#search-product-names-input').on('focus', function(){
                $(this).autocomplete({
                    serviceUrl: function(){
                        return productNamesAndQuantityUrl + '?sKeyword=' + $(this).val();
                    },
                    onSearchComplete: function(query, suggestion){
                        console.log(suggestion);
                        $('.search-and-add-modal #search-add-modal-table-body').html('');

                        if(suggestion.length === 0){
                            $.get(productNamesAndQuantityUrl, function (data) {
                                $('.search-and-add-modal #search-add-modal-table-body').html('');
                                $.each(data.suggestions, function(key, value){
                                    if(value.data.name !== $('#name').val()){
                                        generateSearchedProducts(value.data.id, value.data.name, value.data.quantity_on_hand, value.data.product_type);
                                    }
                                });
                            })
                        }else{
                            $.each(suggestion, function(key, value){
                                if(value.data.name !== $('#name').val()){
                                    generateSearchedProducts(value.data.id, value.data.name, value.data.quantity_on_hand,value.data.product_type)
                                }
                            });
                        }
                    }
                });
            });

            $('#add-bom-btn').click(function(){
                $.get(productNamesAndQuantityUrl, function (data) {
                    $('.search-and-add-modal #search-add-modal-table-body').html('');
                    $.each(data.suggestions, function(key, value){
                        if(value.data.name !== $('#name').val()) {
                            generateSearchedProducts(value.data.id, value.data.name, value.data.quantity_on_hand, value.data.product_type);
                        }
                    });
                })
            });


            $('#search-add-modal-table-body').on('click', '.search-add-use-btn', function(){
                $(this).html('Used').prop('disabled', true);
                let oParent = $(this).parents('.search-add-info');
                generateBomInput(oParent.data('id'), oParent.find('.search-add-info-name').html());
            });

            $('#bom-body').on('click', '.bom-delete', function(){
                $(this).parents('.form-row').remove();
            });

        });

        function generateSearchedProducts(id, name, quantity, type) {
            let btnTxt = 'Use';
            let disabled = '';
            if(quantity){
                quantity = parseFloat(quantity).toString();
            }
            if(quantity === null || quantity === '0'){
                quantity = '<span class="badge badge-danger">Out of stock</span>';
            }
            if(type === 'Consumable'){
                quantity = '<span class="badge badge-info">Consumable</span>';
            }else if(type === 'Service'){
                quantity = '<span class="badge badge-info">Service</span>';
            }
            $('.bom-name').filter(function() {
                if($(this).val() === name){
                    btnTxt = 'Used';
                    disabled = 'Disabled';
                }
            });
            $('.search-and-add-modal #search-add-modal-table-body').append(
                '<tr class="search-add-info" data-id="' + id+ '">' +
                '<td class="text-center search-add-info-name">' + name + '</td>' +
                '<td class="text-center">' + quantity  + '</td>' +
                '<td class="text-center">' +
                '<button style="background-color: #00d994; border-color: #00d994;" class="btn btn-info search-add-use-btn" ' + disabled + '>' + btnTxt + '</button>' +
                '</td>' +
                '</tr>'
            );
        }


        function generateBomInput(iId, sName) {
            return $('#bom-body').append('<div class="form-row">' +
                '<div class="form-group col-md-5">' +
                '<input type="hidden" name="bom_id[]" value="'+ iId  +'">' +
                '<input type="text" class="form-control bom-name" placeholder="Name" value="' + sName  + '" readonly>' +
                '</div>' +
                '<div class="form-group col-md-5">' +
                '<input type="number" name="bom_quantity[]" class="form-control" placeholder="Quantity">' +
                '</div>' +
                '<div class="form-group col-md-2 text-center">' +
                '<button class="btn btn-danger bom-delete" type="button">'+
                ' <i class="fas fa-trash"></i>' +
                '</button>' +
                '</div>' +
                '</div>');
        }

    </script>



@endsection






