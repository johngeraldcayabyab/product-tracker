@extends('dashboard.layouts.dashboard_con')


@section('breadcrumbs')
    <!--Dashboard breadcrumbs-->
    <li class="breadcrumb-item active" aria-current="page">Profile</li>
    <!--End of dashboard breadcrumbs-->
@endsection


@section('actions')
    <!--Create and search-->
    <div class="container-fluid p-2 shadow-sm" style="background-color: #FFFFFF;">
        <div class="row no-gutters">
            <div class="col-md-6 offset-md-6 text-right">
                <button class="btn btn-outline-secondary shadow-sm" type="button" data-toggle="modal"
                        data-target="#create-currency">
                    Hello <strong>{{auth()->user()->name}}</strong>
                </button>
            </div>
        </div>
    </div>
    <!--End of create and search-->
@endsection


@section('dashboard_content')


    <!--Error messages-->
    @include('layouts.error_message')
    <!--End of error messages-->


    <div class="row">
        <div class="col-md-6">
            <form method="post" action="{{route('processUpdateProfile')}}">
                <input type="hidden" value="{{auth()->user()->id}}" name="id">
                @csrf
                <div class="card shadow rounded-0">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="username">Username</label>
                                <input class="form-control" placeholder="Username" name="username" id="username"
                                       type="text"
                                       value="{{$oUser->username}}">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="name">Name</label>
                                <input class="form-control" placeholder="Name" name="name" id="name" type="text"
                                       value="{{$oUser->name}}">
                            </div>
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" class="btn btn-success"
                                        style="background-color: #00d994; border-color: #00d994;">Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6">
            <form method="post" action="{{route('processUpdatePassword')}}">
                <input type="hidden" value="{{auth()->user()->id}}" name="id">
                @csrf
                <div class="card shadow rounded-0">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="current-password">Current password</label>
                                <input class="form-control" placeholder="Current password" name="current_password"
                                       id="current-password" type="password">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="new-password">New password</label>
                                <input class="form-control" placeholder="New password" name="new_password"
                                       id="new-password"
                                       type="password">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="confirm-new-password">Confirm new password</label>
                                <input class="form-control" placeholder="Confirm new password"
                                       name="confirm_new_password"
                                       id="confirm-new-password" type="password">
                            </div>
                            <div class="form-group col-md-12 text-right">
                                <button type="submit" class="btn btn-success"
                                        style="background-color: #00d994; border-color: #00d994;">Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection