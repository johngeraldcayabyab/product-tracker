@extends('dashboard.layouts.dashboard_con')


@section('breadcrumbs')
    <!--Dashboard breadcrumbs-->
    <li class="breadcrumb-item active" aria-current="page">Company</li>
    <!--End of dashboard breadcrumbs-->
@endsection

@section('actions')
    <!--Create and search-->
    <div class="container-fluid p-2 shadow-sm" style="background-color: #FFFFFF;">
        <div class="row no-gutters">
            <div class="col-md-6 offset-md-6 text-right">
                <button class="btn btn-outline-secondary shadow-sm" type="button" data-toggle="modal" data-target="#create-category">
                    Hey!
                </button>
            </div>
        </div>
    </div>
    <!--End of create and search-->
@endsection


@section('dashboard_content')





    <!--Error messages-->
    @include('layouts.error_message')
    <!--End of error messages-->


    <div class="row">

        <div class="col-md-4">

            <div class="row">
                <div class="col-md-12">
                    <div class="card rounded-0 shadow">
                        <table class="table" style="margin-bottom: 0;">
                            <thead>
                            <tr>
                                <th></th>
                                <th scope="col" class="text-center">Total</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr class="currency-data-con">
                                <td class="currency_name">Registered users</td>
                                <td class="country_code text-center"><span class="badge badge-info">{{$iNumberOfUsers}}</span></td>
                            </tr>

                            <tr class="currency-data-con">
                                <td class="currency_name">Total products</td>
                                <td class="country_code text-center"><span class="badge badge-success">{{$iNumberOfProducts}}</span></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-md-12 mt-3">
                    <div class="card rounded-0 shadow">
                        <img class="card-img-top p-1" src="{{$companyLogo}}" alt="Card image cap" style="height: 125px;">
                        <div class="card-body">

                            <form method="post" action="{{route('processUpdateCompanyLogo')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" id="company-logo" name="company_logo">
                                    </div>
                                </div>
                                <div class="form-row float-right">
                                    <button type="submit" class="btn btn-success" style="background-color: #00d994; border-color: #00d994;">Update</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div class="col-md-8">
            <div class="card">
                <table class="table rounded-0 shadow" style="margin-bottom: 0;">
                    <thead>
                    <tr>
                        <th scope="col">Responsible</th>
                        <th scope="col">Action</th>
                        <th scope="col">Created at</th>
                    </tr>
                    </thead>
                    <tbody>


                    @foreach($oLogs as $oLog)
                        <tr class="currency-data-con">
                            <td class="currency_name">{{$oLog->responsible}}</td>
                            <td class="country_code">{{$oLog->action}}</td>
                            <td class="currency_symbol">{{$oLog->created_at}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

            <div class="text-center mt-3">
                <div style="display: inline-block;">
                    {{ $oLogs->links() }}
                </div>
            </div>

        </div>




    </div>




@endsection

@section('customJs')
    <script>
        $( document ).ready(function(){

        });


    </script>
@endsection