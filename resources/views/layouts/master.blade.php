@include('layouts.header')


    <main id="main" class="container-fluid" style="padding-left: 0; padding-right: 0;">
        @yield('content')
    </main>

    <div id="nuotron">
        <div class="ellipse"></div>
        <div class="dot"></div>
    </div>

@include('layouts.footer')