<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}" media="screen,projection">
    <link rel="stylesheet type=text/css" href="{{asset('css/main.css')}}">
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
    @yield('customCss')
    {{--<link rel="icon" href="images/favicon.png">--}}
</head>




<body>