@extends('layouts.master')
@section('content')

<div class="container mt-5">
    <div class="row">
        <div class="col-md-4 offset-md-4">

            <!--Company logo-->
            <div class="text-center">
                <img src="{{$companyLogo}}" class="responsive-img" style="width: 300px;">
            </div>
            <!--End of company logo-->

            <div class="card mt-3 mb-3 rounded-0 shadow">
                <div class="card-body">
                    <h5 class="card-title text-center">Greetings!</h5>

                    <!--Login form container-->
                    <form method="post" action="{{route('loginProcess')}}">

                        @csrf

                        <div class="form-group">
                            <label for="username"><i class="fas fa-user"></i> Username</label>
                            <input type="text" class="form-control" id="username" placeholder="Enter username" name="username" autofocus>
                        </div>

                        <div class="form-group">
                            <label for="password"><i class="fas fa-lock"></i> Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Enter password" name="password" required>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">
                                Log in <i class="fas fa-sign-in-alt"></i>
                            </button>
                        </div>
                    </form>
                    <!--End of login form container-->


                </div>
            </div>

            @include('layouts.error_message')

        </div>
    </div>
</div>

@endsection