This is a for fun project to check the limits of my skill on creating an Inventory system,
but ended up with a simple product CRUD. In short, this project is a DISASTER and it failed.
But if anyone wants to download, test, or try this project.
Follow the instructions below.

Notes:
(I don't really know the name of this project)
The instructions below assumes that you have a working knowledge in GIT, Laravel, MYSQL and basic terminal commands.
Open terminal or command line then type the following commands below
(You must have git and mysql installed)

1) git clone https://github.com/johngeraldcayabyab/Product-tracker.git
2) cd Product-tracker
3) cp .env.example .env
1) mysql -u root -p
2) create database liber
3) exit
4) php artisan key:generate
5) composer install
6) npm install
7) php artisan migrate:fresh --seed
8) php artisan storage:link
9) composer dump-autoload
10) npm run prod
11) php artisan config:cache
12) php artisan serve

Open a browser then go to localhost:8000
Port 8000 is the default port when executing the php artisan serve

Notes: I don't plan on touching the source code of this project again, ever!

Feel free to criticize. Thanks for stopping by!